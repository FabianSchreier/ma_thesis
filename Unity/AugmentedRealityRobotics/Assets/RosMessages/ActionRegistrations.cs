using RosMessages.AugmentedRealityRobotics;
using RosMessages.Control.action;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;

namespace RosMessages
{
    public class ActionRegistrations
    {
#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(FeedbackJointControllerAction.k_RosMessageName,
                FeedbackJointControllerAction.Deserialize);
            MessageRegistry.Register(FeedbackJointControllerActionGoal.k_RosMessageName,
                FeedbackJointControllerActionGoal.Deserialize);
            MessageRegistry.Register(FeedbackJointControllerActionFeedback.k_RosMessageName,
                FeedbackJointControllerActionFeedback.Deserialize);
            MessageRegistry.Register(FeedbackJointControllerActionResult.k_RosMessageName,
                FeedbackJointControllerActionResult.Deserialize);

            MessageRegistry.Register(TestActionAction.k_RosMessageName, TestActionAction.Deserialize);
            MessageRegistry.Register(TestActionActionGoal.k_RosMessageName, TestActionActionGoal.Deserialize);
            MessageRegistry.Register(TestActionActionFeedback.k_RosMessageName, TestActionActionFeedback.Deserialize);
            MessageRegistry.Register(TestActionActionResult.k_RosMessageName, TestActionActionResult.Deserialize);

            MessageRegistry.Register(FollowJointTrajectoryAction.k_RosMessageName,
                FollowJointTrajectoryAction.Deserialize);
            MessageRegistry.Register(FollowJointTrajectoryActionGoal.k_RosMessageName,
                FollowJointTrajectoryActionGoal.Deserialize);
            MessageRegistry.Register(FollowJointTrajectoryActionFeedback.k_RosMessageName,
                FollowJointTrajectoryActionFeedback.Deserialize);
            MessageRegistry.Register(FollowJointTrajectoryActionResult.k_RosMessageName,
                FollowJointTrajectoryActionResult.Deserialize);
        }
    }
}