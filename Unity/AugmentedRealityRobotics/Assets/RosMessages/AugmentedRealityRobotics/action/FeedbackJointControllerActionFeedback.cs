using RosMessageTypes.Actionlib;
using RosMessageTypes.Std;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;

namespace RosMessages.AugmentedRealityRobotics
{
    public class FeedbackJointControllerActionFeedback : ActionFeedback<FeedbackJointControllerFeedback>
    {
        public const string k_RosMessageName = "augmented_reality_robotics/FeedbackJointControllerActionFeedback";
        public override string RosMessageName => k_RosMessageName;


        public FeedbackJointControllerActionFeedback() : base()
        {
            this.feedback = new FeedbackJointControllerFeedback();
        }

        public FeedbackJointControllerActionFeedback(HeaderMsg header, GoalStatusMsg status,
            FeedbackJointControllerFeedback feedback) : base(header, status)
        {
            this.feedback = feedback;
        }

        public static FeedbackJointControllerActionFeedback Deserialize(MessageDeserializer deserializer) =>
            new FeedbackJointControllerActionFeedback(deserializer);

        FeedbackJointControllerActionFeedback(MessageDeserializer deserializer) : base(deserializer)
        {
            this.feedback = FeedbackJointControllerFeedback.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.feedback);
        }
    }
}