using Unity.Robotics.ROSTCPConnector.MessageGeneration;

namespace RosMessages.AugmentedRealityRobotics
{
    public class TestActionAction : Action<TestActionActionGoal, TestActionActionResult, TestActionActionFeedback,
        TestActionGoal, TestActionResult, TestActionFeedback>
    {
        public const string k_RosMessageName = "augmented_reality_robotics/TestActionAction";
        public override string RosMessageName => k_RosMessageName;


        public TestActionAction() : base()
        {
            this.action_goal = new TestActionActionGoal();
            this.action_result = new TestActionActionResult();
            this.action_feedback = new TestActionActionFeedback();
        }

        public static TestActionAction Deserialize(MessageDeserializer deserializer) =>
            new TestActionAction(deserializer);

        TestActionAction(MessageDeserializer deserializer)
        {
            this.action_goal = TestActionActionGoal.Deserialize(deserializer);
            this.action_result = TestActionActionResult.Deserialize(deserializer);
            this.action_feedback = TestActionActionFeedback.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.action_goal);
            serializer.Write(this.action_result);
            serializer.Write(this.action_feedback);
        }
    }
}