using RosMessageTypes.Actionlib;
using RosMessageTypes.Std;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;

namespace RosMessages.AugmentedRealityRobotics
{
    public class TestActionActionFeedback : ActionFeedback<TestActionFeedback>
    {
        public const string k_RosMessageName = "augmented_reality_robotics/TestActionActionFeedback";
        public override string RosMessageName => k_RosMessageName;


        public TestActionActionFeedback() : base()
        {
            this.feedback = new TestActionFeedback();
        }

        public TestActionActionFeedback(HeaderMsg header, GoalStatusMsg status, TestActionFeedback feedback) : base(
            header, status)
        {
            this.feedback = feedback;
        }

        public static TestActionActionFeedback Deserialize(MessageDeserializer deserializer) =>
            new TestActionActionFeedback(deserializer);

        TestActionActionFeedback(MessageDeserializer deserializer) : base(deserializer)
        {
            this.feedback = TestActionFeedback.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.feedback);
        }
    }
}