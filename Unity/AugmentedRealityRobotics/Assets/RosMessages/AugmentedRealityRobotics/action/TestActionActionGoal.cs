using RosMessageTypes.Actionlib;
using RosMessageTypes.Std;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;

namespace RosMessages.AugmentedRealityRobotics
{
    public class TestActionActionGoal : ActionGoal<TestActionGoal>
    {
        public const string k_RosMessageName = "augmented_reality_robotics/TestActionActionGoal";
        public override string RosMessageName => k_RosMessageName;


        public TestActionActionGoal() : base()
        {
            this.goal = new TestActionGoal();
        }

        public TestActionActionGoal(HeaderMsg header, GoalIDMsg goal_id, TestActionGoal goal) : base(header, goal_id)
        {
            this.goal = goal;
        }

        public static TestActionActionGoal Deserialize(MessageDeserializer deserializer) =>
            new TestActionActionGoal(deserializer);

        TestActionActionGoal(MessageDeserializer deserializer) : base(deserializer)
        {
            this.goal = TestActionGoal.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.goal_id);
            serializer.Write(this.goal);
        }
    }
}