using RosMessageTypes.Actionlib;
using RosMessageTypes.Std;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;

namespace RosMessages.AugmentedRealityRobotics
{
    public class FeedbackJointControllerActionGoal : ActionGoal<FeedbackJointControllerGoal>
    {
        public const string k_RosMessageName = "augmented_reality_robotics/FeedbackJointControllerActionGoal";
        public override string RosMessageName => k_RosMessageName;


        public FeedbackJointControllerActionGoal() : base()
        {
            this.goal = new FeedbackJointControllerGoal();
        }

        public FeedbackJointControllerActionGoal(HeaderMsg header, GoalIDMsg goal_id, FeedbackJointControllerGoal goal)
            : base(header, goal_id)
        {
            this.goal = goal;
        }

        public static FeedbackJointControllerActionGoal Deserialize(MessageDeserializer deserializer) =>
            new FeedbackJointControllerActionGoal(deserializer);

        FeedbackJointControllerActionGoal(MessageDeserializer deserializer) : base(deserializer)
        {
            this.goal = FeedbackJointControllerGoal.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.goal_id);
            serializer.Write(this.goal);
        }
    }
}