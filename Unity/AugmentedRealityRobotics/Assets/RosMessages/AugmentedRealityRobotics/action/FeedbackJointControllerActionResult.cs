using RosMessageTypes.Actionlib;
using RosMessageTypes.Std;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;

namespace RosMessages.AugmentedRealityRobotics
{
    public class FeedbackJointControllerActionResult : ActionResult<FeedbackJointControllerResult>
    {
        public const string k_RosMessageName = "augmented_reality_robotics/FeedbackJointControllerActionResult";
        public override string RosMessageName => k_RosMessageName;


        public FeedbackJointControllerActionResult() : base()
        {
            this.result = new FeedbackJointControllerResult();
        }

        public FeedbackJointControllerActionResult(HeaderMsg header, GoalStatusMsg status,
            FeedbackJointControllerResult result) : base(header, status)
        {
            this.result = result;
        }

        public static FeedbackJointControllerActionResult Deserialize(MessageDeserializer deserializer) =>
            new FeedbackJointControllerActionResult(deserializer);

        FeedbackJointControllerActionResult(MessageDeserializer deserializer) : base(deserializer)
        {
            this.result = FeedbackJointControllerResult.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.result);
        }
    }
}