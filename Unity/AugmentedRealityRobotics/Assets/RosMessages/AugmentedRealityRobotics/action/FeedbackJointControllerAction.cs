using Unity.Robotics.ROSTCPConnector.MessageGeneration;

namespace RosMessages.AugmentedRealityRobotics
{
    public class FeedbackJointControllerAction : Action<FeedbackJointControllerActionGoal,
        FeedbackJointControllerActionResult, FeedbackJointControllerActionFeedback, FeedbackJointControllerGoal,
        FeedbackJointControllerResult, FeedbackJointControllerFeedback>
    {
        public const string k_RosMessageName = "augmented_reality_robotics/FeedbackJointControllerAction";
        public override string RosMessageName => k_RosMessageName;


        public FeedbackJointControllerAction() : base()
        {
            this.action_goal = new FeedbackJointControllerActionGoal();
            this.action_result = new FeedbackJointControllerActionResult();
            this.action_feedback = new FeedbackJointControllerActionFeedback();
        }

        public static FeedbackJointControllerAction Deserialize(MessageDeserializer deserializer) =>
            new FeedbackJointControllerAction(deserializer);

        FeedbackJointControllerAction(MessageDeserializer deserializer)
        {
            this.action_goal = FeedbackJointControllerActionGoal.Deserialize(deserializer);
            this.action_result = FeedbackJointControllerActionResult.Deserialize(deserializer);
            this.action_feedback = FeedbackJointControllerActionFeedback.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.action_goal);
            serializer.Write(this.action_result);
            serializer.Write(this.action_feedback);
        }
    }
}