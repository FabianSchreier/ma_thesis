using RosMessageTypes.Actionlib;
using RosMessageTypes.Std;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;

namespace RosMessages.AugmentedRealityRobotics
{
    public class TestActionActionResult : ActionResult<TestActionResult>
    {
        public const string k_RosMessageName = "augmented_reality_robotics/TestActionActionResult";
        public override string RosMessageName => k_RosMessageName;


        public TestActionActionResult() : base()
        {
            this.result = new TestActionResult();
        }

        public TestActionActionResult(HeaderMsg header, GoalStatusMsg status, TestActionResult result) : base(header,
            status)
        {
            this.result = result;
        }

        public static TestActionActionResult Deserialize(MessageDeserializer deserializer) =>
            new TestActionActionResult(deserializer);

        TestActionActionResult(MessageDeserializer deserializer) : base(deserializer)
        {
            this.result = TestActionResult.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.result);
        }
    }
}