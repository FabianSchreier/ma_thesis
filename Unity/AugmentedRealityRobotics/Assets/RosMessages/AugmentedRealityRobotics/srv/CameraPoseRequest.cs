//Do not edit! This file was generated by Unity-ROS MessageGeneration.
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;

namespace RosMessageTypes.AugmentedRealityRobotics
{
    [Serializable]
    public class CameraPoseRequest : Message
    {
        public const string k_RosMessageName = "augmented_reality_robotics/CameraPose";
        public override string RosMessageName => k_RosMessageName;

        public Geometry.PoseStampedMsg target;

        public CameraPoseRequest()
        {
            this.target = new Geometry.PoseStampedMsg();
        }

        public CameraPoseRequest(Geometry.PoseStampedMsg target)
        {
            this.target = target;
        }

        public static CameraPoseRequest Deserialize(MessageDeserializer deserializer) => new CameraPoseRequest(deserializer);

        private CameraPoseRequest(MessageDeserializer deserializer)
        {
            this.target = Geometry.PoseStampedMsg.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.target);
        }

        public override string ToString()
        {
            return "CameraPoseRequest: " +
            "\ntarget: " + target.ToString();
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
