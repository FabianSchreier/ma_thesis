using RosMessageTypes.Actionlib;
using RosMessageTypes.Std;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;

namespace RosMessages.Control.action
{
    public class FollowJointTrajectoryActionResult : ActionResult<FollowJointTrajectoryResult>
    {
        public const string k_RosMessageName = "control_msgs/FollowJointTrajectoryActionResult";
        public override string RosMessageName => k_RosMessageName;


        public FollowJointTrajectoryActionResult() : base()
        {
            this.result = new FollowJointTrajectoryResult();
        }

        public FollowJointTrajectoryActionResult(HeaderMsg header, GoalStatusMsg status, FollowJointTrajectoryResult result) : base(header, status)
        {
            this.result = result;
        }
        public static FollowJointTrajectoryActionResult Deserialize(MessageDeserializer deserializer) => new FollowJointTrajectoryActionResult(deserializer);

        FollowJointTrajectoryActionResult(MessageDeserializer deserializer) : base(deserializer)
        {
            this.result = FollowJointTrajectoryResult.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.result);
        }

    }
}
