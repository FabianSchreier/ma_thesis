using System;
using System.Linq;
using ArrShared;
using ArrShared.Messages;
using ArrShared.Robot;
using RosMessages.Control.action;
using RosMessages.Control.msg;
using RosMessageTypes.Trajectory;
using Shortbit.Utils;
using Unity.Robotics.ROSTCPConnector;
using UnityEngine;
using UnityEngine.Assertions; /*
using ActionServer = ArrShared.Messages.ActionServer<
    RosMessageTypes.AugmentedRealityRobotics.FeedbackJointControllerActionGoal, 
    RosMessageTypes.AugmentedRealityRobotics.FeedbackJointControllerActionResult,
    RosMessageTypes.AugmentedRealityRobotics.FeedbackJointControllerActionFeedback,
    RosMessageTypes.AugmentedRealityRobotics.FeedbackJointControllerGoal, 
    RosMessageTypes.AugmentedRealityRobotics.FeedbackJointControllerResult, 
    RosMessageTypes.AugmentedRealityRobotics.FeedbackJointControllerFeedback>;

using ServerGoalHandle = ArrShared.Messages.ServerGoalHandle<
    RosMessageTypes.AugmentedRealityRobotics.FeedbackJointControllerActionGoal, 
    RosMessageTypes.AugmentedRealityRobotics.FeedbackJointControllerActionResult,
    RosMessageTypes.AugmentedRealityRobotics.FeedbackJointControllerActionFeedback,
    RosMessageTypes.AugmentedRealityRobotics.FeedbackJointControllerGoal, 
    RosMessageTypes.AugmentedRealityRobotics.FeedbackJointControllerResult, 
    RosMessageTypes.AugmentedRealityRobotics.FeedbackJointControllerFeedback>;
    */
using ActionServer = ArrShared.Messages.ActionServer<
    RosMessages.Control.action.FollowJointTrajectoryActionGoal, 
    RosMessages.Control.action.FollowJointTrajectoryActionResult,
    RosMessages.Control.action.FollowJointTrajectoryActionFeedback,
    RosMessages.Control.action.FollowJointTrajectoryGoal, 
    RosMessages.Control.action.FollowJointTrajectoryResult, 
    RosMessages.Control.action.FollowJointTrajectoryFeedback>;

using ServerGoalHandle = ArrShared.Messages.ServerGoalHandle<
    RosMessages.Control.action.FollowJointTrajectoryActionGoal, 
    RosMessages.Control.action.FollowJointTrajectoryActionResult,
    RosMessages.Control.action.FollowJointTrajectoryActionFeedback,
    RosMessages.Control.action.FollowJointTrajectoryGoal, 
    RosMessages.Control.action.FollowJointTrajectoryResult, 
    RosMessages.Control.action.FollowJointTrajectoryFeedback>;

namespace Scripts.Robot
{
    public class FeedbackController : MonoBehaviour
    {
    
        public GameObject robot;
        public string actionServerNamespace = "/augmented_environment/feedback_controller";
        public string mirrorJointStatesTopic = "/joint_states";
    

        public float stiffness;
        public float damping;
        public float forceLimit;
    
    
        private JointMap joints;
    
        private ROSConnection ros;

        private ActionServer actionServer;

        private ServerGoalHandle activeGoal;
        private DateTime activeGoalStart;
        private FollowJointTrajectoryFeedback feedbackMessage;

        private JointStateMirror mirror;
    
        public void Start()
        {
            this.ros = ROSConnection.GetOrCreateInstance();

        
            this.joints = new JointMap(this.robot);
            this.actionServer = new ActionServer(
                this.actionServerNamespace,
                this.GoalCallback, this.CancelCallback);
        
            this.actionServer.Start();
        
            this.mirror = new JointStateMirror(this.joints, this.mirrorJointStatesTopic, this.ros)
            {
                IsEnabled = true,
                Stiffness = this.stiffness,
                Damping = this.damping,
                ForceLimit = this.forceLimit
            };
        }

        private void FixedUpdate()
        {
            this.actionServer.Spin();
            this.UpdateGoal();
        
        }

        private void UpdateGoal()
        {
            if (this.activeGoal == null)
                return;

            Assert.IsNotNull(this.feedbackMessage);
        
            var jointStates = new ActuatingJointStateMap(this.joints, this.activeGoal.Goal.trajectory.joint_names);
        
            var withingPathTolerances = new bool[jointStates.Count];
            var withingGoalTolerances = new bool[jointStates.Count];
        
            for (int i = 0; i < jointStates.Count; i++)
            {
                var state = jointStates[i];
                var jointName = jointStates.JointNames[i];
                var joint = this.joints[jointName];
                var goalTolerance = this.activeGoal.Goal.goal_tolerance.FirstOrDefault(t => t.name == jointName);
                var pathTolerance = this.activeGoal.Goal.path_tolerance.FirstOrDefault(t => t.name == jointName);
            
                state.Desired.Position = joint.TargetPosition;
                state.Desired.Velocity = joint.TargetVelocity;
                state.Actual.Position = joint.CurrentPosition;
                state.Desired.Velocity = joint.CurrentVelocity;
            
                withingGoalTolerances[i] = this.CheckJointTolerances(goalTolerance, state.Error);
                withingPathTolerances[i] = this.CheckJointTolerances(pathTolerance, state.Error);
            
            }
            this.feedbackMessage.UpdateFromJointState(jointStates);
            this.activeGoal.PublishFeedback(this.feedbackMessage);
        
            var trajectoryEnd = this.activeGoalStart
                                + this.activeGoal.Goal.trajectory.points.Last().time_from_start.ToTimeSpan();
            var trajectoryEndWithTolerance = trajectoryEnd
                                             + this.activeGoal.Goal.goal_time_tolerance.ToTimeSpan();

            if (DateTime.UtcNow >= trajectoryEnd)
            {
                if (withingGoalTolerances.All(v => v))
                {
                    Debug.Log("Goal succeeded");
                    this.activeGoal.SetSucceeded(new FollowJointTrajectoryResult()
                    {
                        error_code = FollowJointTrajectoryResult.SUCCESSFUL
                    });
                    this.activeGoal = null;
                }
                else if (DateTime.UtcNow < trajectoryEndWithTolerance)
                {
                    // Still have some time left to meet the goal state tolerances
                }
                else
                {
                    Debug.Log($"Goal failed. Errors: {string.Join(", ", jointStates.Values.Select(s => s.Error.Position))}, withingGoalTolerances: {string.Join(", ", withingGoalTolerances)}");
                    this.activeGoal.SetAborted(new FollowJointTrajectoryResult()
                    {
                        error_code = FollowJointTrajectoryResult.GOAL_TOLERANCE_VIOLATED
                    });
                    this.activeGoal = null;
                }
            }
        }
        private bool CheckJointTolerances(JointToleranceMsg tolerances, ActuatingJointParameters error)
        {
            if (tolerances == null)
            {
                Debug.Log("No tolerances given");
                return true;
            }

            bool isValid = !(tolerances.position     > 0.0 && error.Position     > tolerances.position) &&
                           !(tolerances.velocity     > 0.0 && error.Velocity     > tolerances.velocity) &&
                           !(tolerances.acceleration > 0.0 && error.Acceleration > tolerances.acceleration);
            return isValid;
        }

        public void OnDestroy()
        {
            if (this.actionServer != null)
                this.actionServer.Dispose();
        
            this.mirror.IsEnabled = false;
        }


        private void GoalCallback(ServerGoalHandle goal)
        {
            this.activeGoal = goal;
            this.activeGoal.SetAccepted();
            this.activeGoalStart = DateTime.UtcNow;
        
            Debug.Log($"Got new goal at time {this.activeGoalStart}" );
        
            var numJoints = this.activeGoal.Goal.trajectory.joint_names.Length;
        
            this.feedbackMessage = new FollowJointTrajectoryFeedback
            {
                joint_names = this.joints.Keys.ToArraySafe(),
                desired = new JointTrajectoryPointMsg()
                {
                    positions = new double[numJoints],
                    velocities = new double[numJoints],
                    
                    time_from_start = new DurationMsgFix()
                },
                actual = new JointTrajectoryPointMsg()
                {
                    positions = new double[numJoints],
                    velocities = new double[numJoints],
                    
                    time_from_start = new DurationMsgFix()
                },
                error = new JointTrajectoryPointMsg()
                {
                    positions = new double[numJoints],
                    velocities = new double[numJoints],
                    
                    time_from_start = new DurationMsgFix()
                },
            };
        
        }
        private void CancelCallback(ServerGoalHandle goal)
        {
            if ((this.activeGoal == null) || !goal.Equals(this.activeGoal))
                return;
        
            Debug.Log("Goal canceled");

            this.activeGoal.SetCanceled(new FollowJointTrajectoryResult()
            {
                error_code = FollowJointTrajectoryResult.SUCCESSFUL
            });
            this.activeGoal = null;
        
        }
    }
}