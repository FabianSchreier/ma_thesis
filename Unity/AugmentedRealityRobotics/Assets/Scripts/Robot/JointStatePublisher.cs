using ArrShared;
using ArrShared.Robot;
using RosMessageTypes.Sensor;
using Shortbit.Utils;
using Unity.Robotics.ROSTCPConnector;
using UnityEngine;

namespace Scripts.Robot
{
    public class JointStatePublisher : MonoBehaviour
    {
        public GameObject Robot;

        public string topic = "/augmented_environment/joint_states/simulated";

        public int publishRate = 10;
    
        private ROSConnection ros;
    
        private JointMap joints;

        private Rate rate;

        private JointStateMsg message;
    
        void Start()
        {
        
            this.ros = ROSConnection.GetOrCreateInstance();

            this.joints = new JointMap(this.Robot);
            this.rate = new Rate(this.publishRate);
        
            this.ros.RegisterPublisher<JointStateMsg>(this.topic);

            var numJoints = this.joints.Count;
            this.message = new JointStateMsg
            {
                header =
                {
                    frame_id = "world"
                },
                name = this.joints.Keys.ToArraySafe(),
                position = new double[numJoints],
                velocity = new double[numJoints],
                effort = new double[numJoints]
            };
        }

        private void FixedUpdate()
        {
            this.PublishJointStates();
        }

        private void PublishJointStates()
        {
            Rate.AssureRateInstance(ref this.rate, this.publishRate);
            if (!this.rate.Tick())
                return;

            for (int i = 0; i < this.message.name.Length; i++)
            {
                var joint = this.joints[this.message.name[i]];
                this.message.position[i] = joint.CurrentPosition;
                this.message.velocity[i] = joint.CurrentVelocity;
            }
        
            this.ros.Publish(this.topic, this.message);
        }
    }
}

