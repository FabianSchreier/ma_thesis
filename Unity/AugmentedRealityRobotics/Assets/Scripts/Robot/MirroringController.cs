using ArrShared.Robot;
using Unity.Robotics.ROSTCPConnector;
using UnityEngine;

namespace Scripts.Robot
{
    public class MirroringController : MonoBehaviour
    {
        // ROS Connector
        private ROSConnection ros;
    
        public GameObject Robot;

        public string topic = "/augmented_environment/joint_states/real";
    
        public float stiffness;
        public float damping;
        public float forceLimit;

        private JointMap joints;

        private JointStateMirror mirror;
    
        // Start is called before the first frame update
        void Start()
        {
            this.ros = ROSConnection.GetOrCreateInstance();

            this.joints = new JointMap(this.Robot);

            this.mirror = new JointStateMirror(this.joints, this.topic, this.ros)
            {
                IsEnabled = false,
                Stiffness = this.stiffness,
                Damping = this.damping,
                ForceLimit = this.forceLimit
            };

        }

        private void OnEnable()
        {
            this.mirror.IsEnabled = true;
        }

        private void OnDisable()
        {
            this.mirror.IsEnabled = false;
        }
    }
}
