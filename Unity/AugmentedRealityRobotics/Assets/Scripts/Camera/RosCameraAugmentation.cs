using ArrShared.Graphics;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace Scripts.Camera
{
    public class RosCameraAugmentation : MonoBehaviour
    {
        public string inputCameraTopic;

        public string outputCameraTopic;

        public float outputCameraFov;

        public RosImageEncoding outputEncoding = RosImageEncoding.RGBA8;

        public UnityEngine.Camera objectsCamera;
        public UnityEngine.Camera foregroundMaskCamera;

        private RosCameraSubscriber subscriber;
        private RosCameraPublisher publisher;
        private ImageProcessing processing;
        private RosImageConverter converter;

        private Texture2D rosInputTexture;
        private RenderTexture inputTexture;
        private RenderTexture maskTexture;
        private RenderTexture objectsTexture;
        private RenderTexture maskedObjectsTexture;
        private RenderTexture outputTexture;
        private Texture2D rosOutputTexture;

        public RenderTexture output;
    
    
        // Start is called before the first frame update
        void Start()
        {
            this.subscriber = new RosCameraSubscriber(this.inputCameraTopic);

            this.processing = new ImageProcessing();
            this.converter = new RosImageConverter();

        }

        // Update is called once per frame
        void Update()
        {
            if (!this.subscriber.HasMessage)
                return;
        
            if (this.publisher == null)
            {
                Debug.Log("Initializing processing");
                this.rosInputTexture = this.subscriber.GetLastMessageAsTexture();

                int w = this.rosInputTexture.width;
                int h = this.rosInputTexture.height;

                var outputGraphicsFormat =
                    GraphicsFormatUtility.GetGraphicsFormat(this.outputEncoding.TextureFormat(), true);
            
                this.inputTexture = new RenderTexture(w, h, 0, this.rosInputTexture.graphicsFormat);
                this.inputTexture.Create();
            
                this.maskTexture = new RenderTexture(w, h, 0, outputGraphicsFormat);
                this.maskTexture.Create();
            
                this.objectsTexture = new RenderTexture(w, h, 0, outputGraphicsFormat);
                this.objectsTexture.Create();
            
                this.maskedObjectsTexture = new RenderTexture(w, h, 0, outputGraphicsFormat);
                this.maskedObjectsTexture.Create();
            
                this.outputTexture = new RenderTexture(w, h, 0, outputGraphicsFormat);
                this.outputTexture.Create();
            
                this.rosOutputTexture = new Texture2D(w, h, this.outputEncoding.TextureFormat(), true);

                this.foregroundMaskCamera.targetTexture = this.maskTexture;
                this.objectsCamera.targetTexture = this.objectsTexture;

                this.publisher = new RosCameraPublisher(this.outputCameraTopic, this.subscriber.LastMsg.header.frame_id,
                                                        this.outputEncoding, this.outputCameraFov, this.rosOutputTexture);
            }
            else
                this.subscriber.GetLastMessageAsTexture(this.rosInputTexture);

            this.converter.RosToUnity(this.rosInputTexture, this.inputTexture, this.subscriber.Encoding);
        
            this.processing.AlphaMask(this.maskTexture, this.maskTexture);
            this.processing.InvertMask(this.maskTexture, this.maskTexture);
            this.processing.ApplyMask(this.objectsTexture, this.maskTexture, this.maskedObjectsTexture);
            this.processing.Merge(this.inputTexture, this.maskedObjectsTexture, this.outputTexture);


            //GraphicsUtils.RenderTextureToTexture2D(this.objectsTexture, this.rosOutputTexture);
            //System.IO.File.WriteAllBytes("/home/fabian/projects/thesis/objectsTexture.png", this.rosOutputTexture.EncodeToPNG());
        
            //GraphicsUtils.RenderTextureToTexture2D(this.inputTexture, this.rosOutputTexture);
            //System.IO.File.WriteAllBytes("/home/fabian/projects/thesis/inputTexture.png", this.rosOutputTexture.EncodeToPNG());
        
            //GraphicsUtils.RenderTextureToTexture2D(this.maskedObjectsTexture, this.rosOutputTexture);
            //System.IO.File.WriteAllBytes("/home/fabian/projects/thesis/maskedObjectsTexture.png", this.rosOutputTexture.EncodeToPNG());
        
            //GraphicsUtils.RenderTextureToTexture2D(this.outputTexture, this.rosOutputTexture);
            //System.IO.File.WriteAllBytes("/home/fabian/projects/thesis/outputTexture.png", this.rosOutputTexture.EncodeToPNG());
            
            this.converter.UnityToRos(this.outputTexture, this.rosOutputTexture, this.outputEncoding);
        
            //Graphics.Blit(this.outputTexture, this.output);

            this.publisher.Publish();
        }

        void OnDisable()
        {
            if (this.inputTexture != null)
                this.inputTexture.Release(); 
            if (this.maskTexture != null)
                this.maskTexture.Release(); 
            if (this.objectsTexture != null)
                this.objectsTexture.Release();
            if (this.outputTexture != null)
                this.outputTexture.Release();
            if (this.rosInputTexture != null)
                Destroy(this.rosInputTexture);
            if (this.rosOutputTexture != null)
                Destroy(this.rosOutputTexture);
        }
    }
}
