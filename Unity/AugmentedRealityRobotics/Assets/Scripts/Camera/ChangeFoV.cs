using System.Collections;
using System.Collections.Generic;
using Shortbit.Utils;
using UnityEngine;

public class ChangeFoV : MonoBehaviour
{

    [Range(0f, 180f)]
    public float fov = 75;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        foreach (var cam in this.GetComponentsInChildren<Camera>())
        {
            cam.fieldOfView = this.fov;
        }
    }
}
