using ArrShared;
using ArrShared.Graphics;
using UnityEngine;

namespace Scripts.Camera
{
    public class RosCameraPublish : MonoBehaviour
    {
    
        public string topic;
    
        public string frameId;

        public RosImageEncoding encoding = RosImageEncoding.RGBA8;

        public float cameraFov = 75;

        public new UnityEngine.Camera camera;

        public int width = 2048;
        public int height = 1536;
        public int publishRate = 10;

        private RosCameraPublisher publisher;
        private RosImageConverter converter;

        private RenderTexture unityTexture;
        private Texture2D rosTexture;

        private Rate rate;
    
        // Start is called before the first frame update
        void Start()
        {
            if (this.camera == null)
                this.camera = this.GetComponent<UnityEngine.Camera>();

            this.converter = new RosImageConverter();

            this.rosTexture = new Texture2D(this.width, this.height, this.encoding.TextureFormat(), false);
            this.unityTexture = new RenderTexture(this.width, this.height, 0, this.rosTexture.graphicsFormat);
        
            this.publisher = new RosCameraPublisher(this.topic, this.frameId, this.encoding, this.cameraFov, this.rosTexture);

            this.camera.targetTexture = this.unityTexture;
            
            this.rate = new Rate(this.publishRate);
        }

        // Update is called once per frame
        void Update()
        {
            Rate.AssureRateInstance(ref this.rate, this.publishRate);
            if (!this.rate.Tick())
                return;

            this.converter.UnityToRos(this.unityTexture, this.rosTexture, this.encoding);
            this.publisher.Publish();
        }

        private void OnDisable()
        {
            this.unityTexture.Release();
        }
    }
}
