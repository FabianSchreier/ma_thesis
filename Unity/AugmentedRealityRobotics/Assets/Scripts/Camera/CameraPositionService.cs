using ArrShared;
using RosMessageTypes.AugmentedRealityRobotics;
using RosMessageTypes.Geometry;
using RosMessageTypes.Std;
using Unity.Robotics.ROSTCPConnector;
using Unity.Robotics.ROSTCPConnector.ROSGeometry;
using UnityEngine;

namespace Scripts.Camera
{
    public class CameraPositionService : MonoBehaviour
    {
        public string topic = "/augmented_environment/camera/update";

        [Tooltip("Camera root GameObject to use for this service. If none is given, this GameObject is used.")]
        public GameObject cameraRoot;
    
        void Start()
        {

            if (cameraRoot == null)
                cameraRoot = this.gameObject;
            
            
            var ros = ROSConnection.GetOrCreateInstance();
        
            //ros.RegisterRosService<CameraPoseRequest, CameraPoseResponse>(this.topic);
            ros.ImplementService<CameraPoseRequest, CameraPoseResponse>(this.topic, UpdateCameraPoseCallback);
        }

        private CameraPoseResponse UpdateCameraPoseCallback(CameraPoseRequest arg)
        {
            Debug.Log($"Received new camera pose {arg.target.pose}.");
            this.cameraRoot.transform.UpdateFromPose<FLU>(arg.target.pose);
            return new CameraPoseResponse()
            {
                result = new PoseStampedMsg()
                {
                    header =
                    {
                        frame_id = arg.target.header.frame_id,
                    },
                    pose = this.cameraRoot.transform.ToPose<FLU>()
                }
            };
        }
    }
}
