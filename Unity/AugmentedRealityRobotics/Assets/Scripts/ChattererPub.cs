using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using RosMessageTypes.Std;
using Unity.Robotics.ROSTCPConnector;
using UnityEngine;

public class ChattererPub : MonoBehaviour
{
    private ROSConnection ros;
    
    // Start is called before the first frame update
    void Start()
    {
        this.ros = ROSConnection.GetOrCreateInstance();

        this.ros.RegisterPublisher<StringMsg>("/chatterer");
    }

    // Update is called once per frame
    void Update()
    {
        this.ros.Publish("/chatterer", new StringMsg("Foo" + DateTime.UtcNow.ToString(CultureInfo.InvariantCulture)));
    }
}
