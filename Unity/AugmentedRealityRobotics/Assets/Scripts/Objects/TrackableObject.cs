using UnityEngine;

namespace Scripts.Objects
{
    [DisallowMultipleComponent]
    public class TrackableObject : MonoBehaviour
    {
        public string id;
    }
}
