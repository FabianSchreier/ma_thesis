using System.Linq;
using ArrShared;
using RosMessages.AugmentedRealityRobotics.srv;
using Unity.Robotics.ROSTCPConnector;
using Unity.Robotics.ROSTCPConnector.ROSGeometry;
using UnityEngine;

namespace Scripts.Objects
{
    public class ObjectStateUpdater : MonoBehaviour
    {
        public string topic = "/augmented_environment/objects/update";

        [Tooltip("Object tracker to use for this publisher. If none is given, the object tracker on this GameObject is used.")]
        public ObjectTracker objectTracker;
        
        private ROSConnection ros;
    
        // Start is called before the first frame update
        void Start()
        {
            if (this.objectTracker == null)
                this.objectTracker = this.GetComponent<ObjectTracker>();
        
            this.ros = ROSConnection.GetOrCreateInstance();

            this.ros.ImplementService<UpdateObjectStateRequest, UpdateObjectStateResponse>(this.topic, this.UpdateStateCallback);
        }

        private UpdateObjectStateResponse UpdateStateCallback(UpdateObjectStateRequest request)
        {
            var obj = this.objectTracker.GetObjects(true).FirstOrDefault(o => o.id == request.@object.object_id);

            if (obj == null)
            {
                return new UpdateObjectStateResponse
                {
                    successful = false,
                    error_message = $"Object with id {request.@object.object_id} not found."
                };
            }
            var go = obj.gameObject;
            var rigidBody = obj.GetComponent<Rigidbody>();
            
            go.transform.UpdateFromPose<FLU>(request.@object.pose);
            
            go.SetActive(request.@object.is_enabled);
            if (rigidBody != null)
                rigidBody.isKinematic = request.@object.is_kinematic;
            
            return new UpdateObjectStateResponse
            {
                successful = true
            };
        }
    }
}
