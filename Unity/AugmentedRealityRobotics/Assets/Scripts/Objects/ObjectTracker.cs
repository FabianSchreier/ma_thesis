using System.Collections.Generic;
using System.Linq;
using ArrShared.Attributes;
using Shortbit.Utils;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Scripts.Objects
{
    [DisallowMultipleComponent]
    [ExecuteInEditMode]
    public class ObjectTracker : MonoBehaviour
    {
        public TrackableObject[] explicitObjectList;
        public bool explicitListIsWhitelist = false;
        public bool onlySearchChildren = true;
        
    
        [ReadOnly]
        public TrackableObject[] trackedObjects;
    
        public void Update()
        {
            this.trackedObjects = this.GetObjects().ToArraySafe();
        }


        public IReadOnlyList<TrackableObject> GetObjects(bool includeInactive = false)
        {
            if (this.explicitListIsWhitelist)
                return this.explicitObjectList;

            var blackList = new HashSet<TrackableObject>(this.explicitObjectList);

            var objects = this.onlySearchChildren ? this.GetComponentsInChildren<TrackableObject>(includeInactive) : Object.FindObjectsOfType<TrackableObject>(includeInactive);
            if (blackList.Count == 0)
                return objects;
        
            return objects.Where(o => !blackList.Contains(o)).ToArraySafe();
        }
    }
}
