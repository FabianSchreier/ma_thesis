using System.Collections.Generic;
using ArrShared;
using RosMessages.AugmentedRealityRobotics.msg;
using Shortbit.Utils;
using Unity.Robotics.ROSTCPConnector;
using Unity.Robotics.ROSTCPConnector.ROSGeometry;
using UnityEngine;

namespace Scripts.Objects
{
    public class ObjectStatePublisher : MonoBehaviour
    {

        public string topic = "/augmented_environment/objects/state";
        public int publishRate = 10;

        [Tooltip("Object tracker to use for this publisher. If none is given, the object tracker on this GameObject is used.")]
        public ObjectTracker objectTracker;
        
        private Rate rate;
        private ROSConnection ros;
        private ObjectStateArrayStampedMsg message;
    
        private void Start()
        {
            if (this.objectTracker == null)
                this.objectTracker = this.GetComponent<ObjectTracker>();
        
            this.ros = ROSConnection.GetOrCreateInstance();
        
            this.rate = new Rate(this.publishRate);
            this.message = new ObjectStateArrayStampedMsg();
        
            this.ros.RegisterPublisher<ObjectStateArrayStampedMsg>(this.topic);
            
        }

        private void FixedUpdate()
        {
            this.PublishPoses();
        }

        private void PublishPoses()
        {
            Rate.AssureRateInstance(ref this.rate, this.publishRate);
            if (!this.rate.Tick())
                return;
        
            var objectMsgs = new List<ObjectStateMsg>();
            foreach (var obj in this.objectTracker.GetObjects(true))
            {
                var go = obj.gameObject;
                var rigidBody = go.GetComponent<Rigidbody>();
                
                var msg = new ObjectStateMsg
                {
                    object_id = obj.id,
                    is_enabled = go.activeInHierarchy,
                    has_rigid_body = rigidBody != null,
                    is_kinematic = rigidBody != null && rigidBody.isKinematic,
                    
                    pose = obj.transform.ToPose<FLU>()
                };
                objectMsgs.Add(msg);
            }

            this.message.header.stamp = TimeMsgEx.Now();
            this.message.object_state_array.objects = objectMsgs.ToArraySafe();
            this.ros.Publish(this.topic, this.message);
        }
    }
}
