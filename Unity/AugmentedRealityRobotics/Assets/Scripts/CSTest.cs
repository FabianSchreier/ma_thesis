using RosMessages.AugmentedRealityRobotics;
using UnityEngine;
using ActionServer = ArrShared.Messages.ActionServer<
    RosMessages.AugmentedRealityRobotics.TestActionActionGoal, 
    RosMessages.AugmentedRealityRobotics.TestActionActionResult, 
    RosMessages.AugmentedRealityRobotics.TestActionActionFeedback, 
    RosMessages.AugmentedRealityRobotics.TestActionGoal, 
    RosMessages.AugmentedRealityRobotics.TestActionResult, 
    RosMessages.AugmentedRealityRobotics.TestActionFeedback>;

using ServerGoalHandle = ArrShared.Messages.ServerGoalHandle<
    RosMessages.AugmentedRealityRobotics.TestActionActionGoal, 
    RosMessages.AugmentedRealityRobotics.TestActionActionResult, 
    RosMessages.AugmentedRealityRobotics.TestActionActionFeedback, 
    RosMessages.AugmentedRealityRobotics.TestActionGoal, 
    RosMessages.AugmentedRealityRobotics.TestActionResult, 
    RosMessages.AugmentedRealityRobotics.TestActionFeedback>;

namespace Scripts
{
    public class CSTest : MonoBehaviour
    {
        private ActionServer actionServer;

        private ServerGoalHandle currentHandle;
        private int index;

        void Start()
        {
            this.actionServer = new ActionServer("/TestAction", this.GoalCallback, this.CancelCallback);
            this.actionServer.Start();
        }


        private void GoalCallback(ServerGoalHandle gh)
        {
            gh.SetAccepted();
            this.index = 0;
            this.currentHandle = gh;
            Debug.Log($"Got goal {gh.Goal.goal} with id {gh.GoalID.id}");
        }

        private void CancelCallback(ServerGoalHandle gh)
        {
        }
    
        private void Update()
        {
            if (this.currentHandle == null)
                return;
        
            if (this.index < 10)
            {
                this.index++;
                this.currentHandle.PublishFeedback(new TestActionFeedback("feedback "+ this.index));
            }
            else
            {
                this.currentHandle.SetSucceeded(new TestActionResult("finished!"));
                this.currentHandle = null;
            }
        }

        void OnDisable()
        {
        }
    }
}