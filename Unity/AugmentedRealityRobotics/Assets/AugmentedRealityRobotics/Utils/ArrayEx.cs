namespace ArrShared.Utils
{
    public static class ArrayEx
    {
        public static void AssureLength<T>(ref T[] array, int length)
        {
            if (array == null || array.Length != length)
                array = new T[length];
        }
        
    }
}