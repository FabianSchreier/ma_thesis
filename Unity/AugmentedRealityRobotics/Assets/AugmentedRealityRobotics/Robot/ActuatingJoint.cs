using System;
using Unity.Robotics.UrdfImporter;
using UnityEngine;

namespace ArrShared.Robot
{
    public class ActuatingJoint
    {
        public ActuatingJoint(string name, UrdfJoint urdfJoint, ArticulationBody articulationBody)
        {
            this.UrdfJoint = urdfJoint;
            this.ArticulationBody = articulationBody;
            this.Name = name;

            switch (this.UrdfJoint.JointType)
            {
                case UrdfJoint.JointTypes.Revolute:
                    this.JointType = ActuatingJointTypes.Revolute;
                    break;
                case UrdfJoint.JointTypes.Prismatic:
                    this.JointType = ActuatingJointTypes.Prismatic;
                    break;
                default:
                    throw new ArgumentException($"Given joint {name} has invalid JointType {this.UrdfJoint.JointType}");
            }
        }

        public string Name { get; }
        
        public ActuatingJointTypes JointType { get; }
        
        public UrdfJoint UrdfJoint { get; }
        
        public ArticulationBody ArticulationBody { get; }

        public float CurrentPosition => this.ArticulationBody.jointPosition[0];

        public float TargetPosition
        {
            get
            { 
                var ret = this.ArticulationBody.xDrive.target;
                if (this.JointType == ActuatingJointTypes.Revolute)
                    ret *= Mathf.Deg2Rad;
                return ret;
                
            }
            set
            {
                var xDrive = this.ArticulationBody.xDrive;

                xDrive.target = value;
                if (this.JointType == ActuatingJointTypes.Revolute)
                    xDrive.target *= Mathf.Rad2Deg;

                this.ArticulationBody.xDrive = xDrive;
            }
        }

        public float CurrentVelocity => this.ArticulationBody.jointVelocity[0];

        public float TargetVelocity
        {
            get
            {
                var ret = this.ArticulationBody.xDrive.targetVelocity; 
                if (this.JointType == ActuatingJointTypes.Revolute)
                    ret *= Mathf.Deg2Rad;
                return ret;
            }
            set
            {
                var xDrive = this.ArticulationBody.xDrive;

                xDrive.targetVelocity = value;
                if (this.JointType == ActuatingJointTypes.Revolute)
                    xDrive.targetVelocity *= Mathf.Rad2Deg;

                this.ArticulationBody.xDrive = xDrive;
            }
        }
    }
}