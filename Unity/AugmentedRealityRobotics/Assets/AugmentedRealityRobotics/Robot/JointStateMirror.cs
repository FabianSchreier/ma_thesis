using JetBrains.Annotations;
using RosMessageTypes.Sensor;
using Unity.Robotics.ROSTCPConnector;
using Unity.Robotics.UrdfImporter;
using UnityEngine;
using UnityEngine.Assertions;

namespace ArrShared.Robot
{
	public class JointStateMirror
	{
		public JointMap Joints { get; }
		public string JointStatesTopic { get; }


		public float Stiffness
		{
			get => this.stiffness;
			set
			{
				this.stiffness = value; 
				this.UpdatePhysicalParameters();
			}
		}

		public float Damping
		{
			get => this.damping;
			set
			{
				this.damping = value;
				this.UpdatePhysicalParameters();
			}
		}

		public float ForceLimit
		{
			get => this.forceLimit;
			set
			{
				this.forceLimit = value;
				this.UpdatePhysicalParameters();
			}
		}
		
		[CanBeNull]
		public JointStateMsg LastReceivedMessage { get; private set; }

		public bool IsEnabled { get; set; } = true;

		private readonly ROSConnection ros;
		private float stiffness = 1000;
		private float damping = 100;
		private float forceLimit = 1000;

		public JointStateMirror(JointMap joints, string jointStatesTopic, ROSConnection ros = null)
		{
			this.Joints = joints;
			this.JointStatesTopic = jointStatesTopic;
			
			this.ros = ros != null ? ros : ROSConnection.GetOrCreateInstance();
        
			this.ros.Subscribe<JointStateMsg>(this.JointStatesTopic, JointStatesCallback);
			this.UpdatePhysicalParameters();
		}

		private void UpdatePhysicalParameters()
		{
			foreach (var join in this.Joints.Values)
			{
				var drive = join.ArticulationBody.xDrive;

				drive.damping = this.Damping;
				drive.stiffness = this.Stiffness;
				drive.forceLimit = this.ForceLimit;
				join.ArticulationBody.xDrive = drive;
			}
		}
		
		private void JointStatesCallback(JointStateMsg obj)
		{
			this.LastReceivedMessage = obj;
			
			if (!this.IsEnabled)
				return;
			
			Assert.AreEqual(obj.name.Length, obj.position.Length);
			if (obj.velocity?.Length > 0)
				Assert.AreEqual(obj.name.Length, obj.velocity.Length);
        
			for (int i = 0; i < obj.name.Length; i++)
			{
				var name = obj.name[i];
				var position = obj.position[i];
				var velocity = 0.0; 
				if (obj.velocity?.Length > 0)
					velocity = obj.velocity[i];

				if (!this.Joints.ContainsKey(name))
				{
					Debug.Log($"Skipped unknown joint {name}");
					continue;
				}

				var joint = this.Joints[name];


				var xDrive = joint.ArticulationBody.xDrive;
            
				xDrive.target = (float) position;
				xDrive.targetVelocity = (float) velocity;
				if (joint.UrdfJoint.JointType == UrdfJoint.JointTypes.Revolute)
				{
					xDrive.target *= Mathf.Rad2Deg;
					xDrive.targetVelocity *= Mathf.Rad2Deg;
				}
            
				this.Joints[name].ArticulationBody.xDrive = xDrive;
			}
		}
	}
}