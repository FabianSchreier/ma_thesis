using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ArrShared.Robot
{
    public class ActuatingJointStateMap : IReadOnlyDictionary<string, ActuatingJointState>, IReadOnlyList<ActuatingJointState>
    {
        private readonly Dictionary<string, ActuatingJointState> jointStates;
        private readonly string[] jointOrder;


        public ActuatingJointStateMap(JointMap joints, string[] jointOrder)
        {
            this.jointOrder = jointOrder;
            this.jointStates = new Dictionary<string, ActuatingJointState>();
            foreach (var jointName in this.jointOrder)
            {
                var joint = joints[jointName];
                this.jointStates[jointName] = new ActuatingJointState(joint.JointType, true);
            }
        }

        IEnumerator<ActuatingJointState> IEnumerable<ActuatingJointState>.GetEnumerator() =>
            this.jointStates.Values.GetEnumerator();

        IEnumerator<KeyValuePair<string, ActuatingJointState>> IEnumerable<KeyValuePair<string, ActuatingJointState>>.
            GetEnumerator() => this.jointStates.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable<ActuatingJointState>)this).GetEnumerator();

        public bool ContainsKey(string key) => this.jointStates.ContainsKey(key);

        public bool TryGetValue(string key, out ActuatingJointState value) =>
            this.jointStates.TryGetValue(key, out value);
        
        public int Count => this.jointStates.Count;

        public ActuatingJointState this[string key] => this.jointStates[key];
        public ActuatingJointState this[int index] => this.jointStates[this.jointOrder[index]];

        public IEnumerable<string> Keys => this.jointStates.Keys;
        public IEnumerable<ActuatingJointState> Values => this.jointStates.Values;

        public IReadOnlyList<string> JointNames => this.jointOrder;


    }
}