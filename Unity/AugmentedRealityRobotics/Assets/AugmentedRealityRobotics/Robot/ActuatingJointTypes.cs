namespace ArrShared.Robot
{
    public enum ActuatingJointTypes
    {
        Revolute,
        Prismatic,
    }
}