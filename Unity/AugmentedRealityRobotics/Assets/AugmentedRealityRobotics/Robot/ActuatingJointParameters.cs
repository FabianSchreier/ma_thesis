using System.ComponentModel;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;

namespace ArrShared.Robot
{
    public class ActuatingJointParameters : INotifyPropertyChanged
    {
        private double position;
        private double velocity;
        private double acceleration;

        public double Position
        {
            get => position;
            set
            {
                position = value;
                this.OnPropertyChanged();
            }
        }

        public double Velocity
        {
            get => velocity;
            set
            {
                velocity = value;
                this.OnPropertyChanged();
            }
        }

        public double Acceleration
        {
            get => acceleration;
            set
            {
                acceleration = value;
                this.OnPropertyChanged();
            }
        }

        public override string ToString()
        {
            return $"{this.Position}, {this.Velocity}, {this.Acceleration}";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}