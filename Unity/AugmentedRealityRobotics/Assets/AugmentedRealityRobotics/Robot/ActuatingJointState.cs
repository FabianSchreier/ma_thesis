using System;
using System.ComponentModel;

namespace ArrShared.Robot
{
    public class ActuatingJointState
    {
        public delegate void ErrorUpdateFunction(ActuatingJointParameters desired, ActuatingJointParameters actuatingJointState,
            ref ActuatingJointParameters error);
        
        private bool stateChanged;
        private readonly bool lazyErrorCalculation;
        private readonly ErrorUpdateFunction errorFunction;
        
        private readonly ActuatingJointParameters desired;
        private readonly ActuatingJointParameters actual;
        private ActuatingJointParameters error;

        public ActuatingJointParameters Desired => desired;

        public ActuatingJointParameters Actual => actual;

        public ActuatingJointParameters Error
        {
            get
            {
                if (this.lazyErrorCalculation && this.stateChanged)
                    this.CalculateError();
                return error;
            }
        }

        public ActuatingJointState(ActuatingJointTypes jointType)
            : this(
                ActuatingJointState.GetDefaultErrorFunction(jointType), true)
        {}
        public ActuatingJointState(ActuatingJointTypes jointType, bool lazyErrorCalculation)
            : this(
                ActuatingJointState.GetDefaultErrorFunction(jointType), lazyErrorCalculation)
        {}
        
        public ActuatingJointState(ErrorUpdateFunction errorFunction, bool lazyErrorCalculation)
        {
            this.errorFunction = errorFunction;
            this.lazyErrorCalculation = lazyErrorCalculation;
            
            this.desired = new ActuatingJointParameters();
            this.actual = new ActuatingJointParameters();
            this.error = new ActuatingJointParameters();
            
            this.desired.PropertyChanged += StateChanged;
            this.actual.PropertyChanged += StateChanged;
            
            this.stateChanged = false;
        }

        private void StateChanged(object sender, PropertyChangedEventArgs e)
        {
            this.stateChanged = true;
            if (!this.lazyErrorCalculation)
                this.CalculateError();
        }

        public void CalculateError()
        {
            this.errorFunction(this.desired, this.actual, ref this.error);
            this.stateChanged = false;
        }

        private static ErrorUpdateFunction
            GetDefaultErrorFunction(ActuatingJointTypes jointType)
        {
            switch (jointType)
            {
                case ActuatingJointTypes.Prismatic:
                    return DefaultPrismaticErrorFunction;
                case ActuatingJointTypes.Revolute:
                    return DefaultRevoluteErrorFunction;
                default:
                    throw new ArgumentException(
                        $"No default error function defined for ActuatingJointType {jointType}.");
            }
        }

        public static void DefaultPrismaticErrorFunction(ActuatingJointParameters desired,
            ActuatingJointParameters actual, ref ActuatingJointParameters error)
        {
            error.Position = Math.Abs(desired.Position - actual.Position);
            error.Velocity = Math.Abs(desired.Velocity - actual.Velocity);
            error.Acceleration = Math.Abs(desired.Acceleration - actual.Acceleration);
        }
        public static void DefaultRevoluteErrorFunction(ActuatingJointParameters desired,
            ActuatingJointParameters actual, ref ActuatingJointParameters error)
        {
            error.Position = Math.Abs(desired.Position - actual.Position);  // TODO: Add angle normalization
            error.Velocity = Math.Abs(desired.Velocity - actual.Velocity);
            error.Acceleration = Math.Abs(desired.Acceleration - actual.Acceleration);
        }
    }
}