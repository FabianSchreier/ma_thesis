using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Robotics.UrdfImporter;
using UnityEngine;

namespace ArrShared.Robot
{
    public class JointMap : IReadOnlyDictionary<string, ActuatingJoint>
    {

        public GameObject Robot { get; }
        private readonly Dictionary<string, ActuatingJoint> nameJointMap;

        public JointMap(GameObject robot)
        {
            this.Robot = robot;
            
            this.nameJointMap = new Dictionary<string, ActuatingJoint>();
            
            var articulateBodies = robot.transform.GetComponentsInChildren<ArticulationBody>();
            foreach (var body in articulateBodies)
            {
                var joint = body.transform.GetComponent<UrdfJoint>();
                if (joint.JointType != UrdfJoint.JointTypes.Fixed)
                {
                    nameJointMap[joint.jointName] = new ActuatingJoint(
                        name: joint.jointName,
                        urdfJoint: joint,
                        articulationBody: body
                    );
                }
            }
        }

        public IEnumerator<KeyValuePair<string, ActuatingJoint>> GetEnumerator() => this.nameJointMap.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

        public int Count => this.nameJointMap.Count;

        public bool ContainsKey(string jointName) => this.nameJointMap.ContainsKey(jointName);

        public bool TryGetValue(string jointName, out ActuatingJoint value) => this.nameJointMap.TryGetValue(jointName, out value);

        public ActuatingJoint this[string jointName] => this.nameJointMap[jointName];

        public IEnumerable<string> Keys => this.nameJointMap.Keys;
        public IEnumerable<ActuatingJoint> Values => this.nameJointMap.Values;
    }
}