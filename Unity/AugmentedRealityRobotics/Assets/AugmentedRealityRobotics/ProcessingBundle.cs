using System.IO;
using System.Linq;
using UnityEngine;

namespace ArrShared
{
	public class ProcessingBundle
	{
		private static ProcessingBundle instance;
		private static readonly object instanceLock = new object();
		
		public static ProcessingBundle Instance
		{
			get
			{
				if (instance == null)
				{
					lock (instanceLock)
					{
						if (instance == null)
						{
							instance = new ProcessingBundle();
						}
					}
				}

				return instance;
			}
		}

		private AssetBundle bundle;

		private ProcessingBundle()
		{
			this.bundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "processingbundle"));
			
			//Debug.Log(string.Join(", ", this.bundle.GetAllAssetNames()));
		}

		public string[] GetAllAssetNames()
		{
			return this.bundle.GetAllAssetNames();
		}
		
		public ComputeShader RosImageComputeShader
		{
			get => this.bundle.LoadAsset<ComputeShader>("assets/bundledassets/processingbundle/rosimage.compute");

		}
		
		public ComputeShader ImageProcessingComputeShader
		{
			get => this.bundle.LoadAsset<ComputeShader>("assets/bundledassets/processingbundle/imageprocessing.compute");

		}
	}
}