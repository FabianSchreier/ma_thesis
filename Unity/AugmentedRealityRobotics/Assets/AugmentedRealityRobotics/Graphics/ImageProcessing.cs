using UnityEditor;
using UnityEngine;

namespace ArrShared.Graphics
{
    public class ImageProcessing
    {
        private ComputeShader cs;


        private readonly int grayscaleMaskKernelIndex;
        private readonly int alphaMaskKernelIndex;
        private readonly int invertMaskKernelIndex;
        private readonly int applyMaskKernelIndex;
        private readonly int mergeKernelIndex;
        
        
        public ImageProcessing()
        {
            this.cs = ProcessingBundle.Instance.ImageProcessingComputeShader;

            this.grayscaleMaskKernelIndex = this.cs.FindKernel("GrayscaleMask");
            this.alphaMaskKernelIndex = this.cs.FindKernel("AlphaMask");
            this.invertMaskKernelIndex = this.cs.FindKernel("InvertMask");
            this.applyMaskKernelIndex = this.cs.FindKernel("ApplyMask");
            this.mergeKernelIndex = this.cs.FindKernel("Merge");
        }


        public void GrayscaleMask(Texture input, RenderTexture output)
        {
            var result = GraphicsUtils.MakeRandomAccessibleCloneIfNecessary(output);
            if (object.ReferenceEquals(input, result))
                result = GraphicsUtils.CreateRandomAccessibleRenderTexture(result);
            
            
            this.cs.SetTexture(this.grayscaleMaskKernelIndex, "InputA", input);
            this.cs.SetTexture(this.grayscaleMaskKernelIndex, "Result", result);
        
            this.cs.Dispatch(this.grayscaleMaskKernelIndex, result.width / 8, result.height / 8, 1);
            GraphicsUtils.CopyFromGeneratedRandomAccessibleAndRelease(result, output);
        }

        public RenderTexture GrayscaleMask(Texture input)
        {
            var output = GraphicsUtils.CreateRandomAccessibleRenderTexture(input);
            this.GrayscaleMask(input, output);
            return output;
        }
        
        public void AlphaMask(Texture input, RenderTexture output)
        {
            var result = GraphicsUtils.MakeRandomAccessibleCloneIfNecessary(output);
            if (object.ReferenceEquals(input, result))
                result = GraphicsUtils.CreateRandomAccessibleRenderTexture(result);
            
            
            this.cs.SetTexture(this.alphaMaskKernelIndex, "InputA", input);
            this.cs.SetTexture(this.alphaMaskKernelIndex, "Result", result);
        
            this.cs.Dispatch(this.alphaMaskKernelIndex, result.width / 8, result.height / 8, 1);
            GraphicsUtils.CopyFromGeneratedRandomAccessibleAndRelease(result, output);
        }

        public RenderTexture AlphaMask(Texture input)
        {
            var output = GraphicsUtils.CreateRandomAccessibleRenderTexture(input);
            this.AlphaMask(input, output);
            return output;
        }
        
        public void InvertMask(Texture input, RenderTexture output)
        {
            var result = GraphicsUtils.MakeRandomAccessibleCloneIfNecessary(output);
            if (object.ReferenceEquals(input, result))
                result = GraphicsUtils.CreateRandomAccessibleRenderTexture(result);
            
            
            this.cs.SetTexture(this.invertMaskKernelIndex, "InputA", input);
            this.cs.SetTexture(this.invertMaskKernelIndex, "Result", result);
        
            this.cs.Dispatch(this.invertMaskKernelIndex, result.width / 8, result.height / 8, 1);
            GraphicsUtils.CopyFromGeneratedRandomAccessibleAndRelease(result, output);
        }

        public RenderTexture InvertMask(Texture input)
        {
            var output = GraphicsUtils.CreateRandomAccessibleRenderTexture(input);
            this.InvertMask(input, output);
            return output;
        }
        
        public void ApplyMask(Texture input, Texture mask, RenderTexture output)
        {
            var result = GraphicsUtils.MakeRandomAccessibleCloneIfNecessary(output);
            if (object.ReferenceEquals(input, result) || object.ReferenceEquals(mask, result) )
                result = GraphicsUtils.CreateRandomAccessibleRenderTexture(result);
            
            
            this.cs.SetTexture(this.applyMaskKernelIndex, "InputA", input);
            this.cs.SetTexture(this.applyMaskKernelIndex, "InputB", mask);
            this.cs.SetTexture(this.applyMaskKernelIndex, "Result", result);
        
            this.cs.Dispatch(this.applyMaskKernelIndex, result.width / 8, result.height / 8, 1);
            GraphicsUtils.CopyFromGeneratedRandomAccessibleAndRelease(result, output);
        }

        public RenderTexture ApplyMask(Texture input, Texture mask)
        {
            var output = GraphicsUtils.CreateRandomAccessibleRenderTexture(input);
            this.ApplyMask(input, mask, output);
            return output;
        }
        
        public void Merge(Texture inputA, Texture inputB, RenderTexture output)
        {
            var result = GraphicsUtils.MakeRandomAccessibleCloneIfNecessary(output);
            if (object.ReferenceEquals(inputA, result) || object.ReferenceEquals(inputB, result) )
                result = GraphicsUtils.CreateRandomAccessibleRenderTexture(result);
            
            
            this.cs.SetTexture(this.mergeKernelIndex, "InputA", inputA);
            this.cs.SetTexture(this.mergeKernelIndex, "InputB", inputB);
            this.cs.SetTexture(this.mergeKernelIndex, "Result", result);
        
            this.cs.Dispatch(this.mergeKernelIndex, result.width / 8, result.height / 8, 1);
            GraphicsUtils.CopyFromGeneratedRandomAccessibleAndRelease(result, output);
        }

        public RenderTexture Merge(Texture inputA, Texture inputB)
        {
            var output = GraphicsUtils.CreateRandomAccessibleRenderTexture(inputA);
            this.Merge(inputA, inputB, output);
            return output;
        }
    }
}