using System;
using RosMessageTypes.Sensor;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace ArrShared.Graphics
{
    public class RosImageConverter
    {

        private readonly ComputeShader computeShader;
        
        private readonly int rosToUnityKernelIndex;
        private readonly int unityToRosKernelIndex;
        
        public RosImageConverter()
        {
            this.computeShader = ProcessingBundle.Instance.RosImageComputeShader;
            
            this.rosToUnityKernelIndex = this.computeShader.FindKernel("RosToUnity");
            this.unityToRosKernelIndex = this.computeShader.FindKernel("UnityToRos");
        }


        public static void RosImageMessageToTexture(ImageMsg input, Texture2D output)
        {
            //output.LoadRawTextureData(input.data);
            output.SetPixelData(input.data, 0);
            
            output.Apply();
        }
        
        public static Texture2D RosImageMessageToTexture(ImageMsg input)
        {
            var msgEncoding = RosImageEncodingEx.RosNameToEncoding[input.encoding];
            var output = new Texture2D((int) input.width, (int) input.height, msgEncoding.TextureFormat(), false);

            RosImageMessageToTexture(input, output);
            
            return output;
        }

        public static void TextureToRosImageMessage(Texture2D input, RosImageEncoding rosImageEncoding, ImageMsg output)
        {
            var rawData = input.GetPixelData<byte>(0);

            if (output.data == null || output.data.Length != rawData.Length)
                output.data = new byte[rawData.Length];
            
            //Buffer.BlockCopy(rawData, 0, output.data, 0, rawData.Length);
            rawData.CopyTo(output.data);
        }

        public static ImageMsg TextureToRosImageMessage(Texture2D input, RosImageEncoding rosImageEncoding)
        {
            var output = new ImageMsg
            {
                width = (uint) input.width,
                height = (uint) input.height,
                encoding = rosImageEncoding.RosName(),
                step = (uint) (input.width * rosImageEncoding.PixelByteSize())
            };

            TextureToRosImageMessage(input, rosImageEncoding, output);

            return output;
        }
        


        public void RosToUnity(Texture2D input, RenderTexture output, RosImageEncoding rosImageEncoding)
        {
            if (input.width != output.width || input.height != output.height)
                throw new ArgumentException("Given input and output textures do not match in size.");
            
            var encodingChannelOrder = rosImageEncoding.ChannelOrder();
            var rosToUnityChannelMapping = new int[] {0, 1, 2, 3};
            
            for (int i = 0; i < encodingChannelOrder.Length; i++)
                rosToUnityChannelMapping[i] = encodingChannelOrder[i];

            var result = GraphicsUtils.MakeRandomAccessibleCloneIfNecessary(output);
        
            this.computeShader.SetTexture(0, "Input", input);
            this.computeShader.SetTexture(0, "Result", result);
            this.computeShader.SetInts( "ChannelMapping", rosToUnityChannelMapping);
            
            this.computeShader.Dispatch(this.rosToUnityKernelIndex, output.width / 8, output.height / 8, 1);

            GraphicsUtils.CopyFromGeneratedRandomAccessibleAndRelease(result, output);
        }

        public RenderTexture RosToUnity(Texture2D input, RosImageEncoding rosImageEncoding)
        {
            var textureFormat = rosImageEncoding.TextureFormat();
            
            var rt = new RenderTexture((int) input.width, (int) input.height, 0, GraphicsFormatUtility.GetGraphicsFormat(textureFormat, true))
            {
                enableRandomWrite = true
            };
            rt.Create();

            this.RosToUnity(input, rt, rosImageEncoding);

            return rt;
        }

        public void UnityToRos(Texture input, Texture2D output, RosImageEncoding rosImageEncoding)
        {
            if (input.width != output.width || input.height != output.height)
                throw new ArgumentException("Given input and output textures do not match in size.");
            
            var encodingChannelOrder = rosImageEncoding.ChannelOrder();
            var unityToRosChannelMapping = new int[] {0, 1, 2, 3};
            
            for (int i = 0; i < encodingChannelOrder.Length; i++)
                unityToRosChannelMapping[encodingChannelOrder[i]] = i;

            var result = GraphicsUtils.CreateRandomAccessibleRenderTexture(output);
            
            this.computeShader.SetTexture(0, "Input", input);
            this.computeShader.SetTexture(0, "Result", result);
            this.computeShader.SetInts( "ChannelMapping", unityToRosChannelMapping);
            
            this.computeShader.Dispatch(this.rosToUnityKernelIndex, output.width / 8, output.height / 8, 1);

            var activeRT = RenderTexture.active;
            RenderTexture.active = result;
            output.ReadPixels(new Rect(0, 0, output.width, output.height), 0, 0);
            RenderTexture.active = activeRT;
            
            GraphicsUtils.ReleaseGeneratedTexture(result);
        }

        public Texture2D UnityToRos(Texture input, RosImageEncoding rosImageEncoding)
        {
            var output = new Texture2D(input.width, input.height, rosImageEncoding.TextureFormat(), false);

            this.UnityToRos(input, output, rosImageEncoding);

            return output;
        }

    }
}