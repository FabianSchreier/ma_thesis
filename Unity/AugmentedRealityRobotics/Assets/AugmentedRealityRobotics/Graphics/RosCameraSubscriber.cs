using System;
using RosMessageTypes.Sensor;
using Unity.Robotics.ROSTCPConnector;
using UnityEditor;
using UnityEngine;

namespace ArrShared.Graphics
{
	public class RosCameraSubscriber
	{
		public string Topic { get; }

		public int Width
		{
			get
			{
				if (!this.HasMessage)
					throw new InvalidOperationException("No message received yet.");

				return (int) this.LastMsg.width;
			}
		}

		public int Height
		{
			get
			{
				if (!this.HasMessage)
					throw new InvalidOperationException("No message received yet.");

				return (int) this.LastMsg.height;
			}
		}

		public RosImageEncoding Encoding
		{
			get
			{
				
				if (!this.HasMessage)
					throw new InvalidOperationException("No message received yet.");

				return RosImageEncodingEx.RosNameToEncoding[this.LastMsg.encoding];
			}
		}

		public bool HasMessage => this.LastMsg != null;

		public ImageMsg LastMsg { get; private set; }
		
		
		public RosCameraSubscriber(string topic)
		{
			this.Topic = topic;

			
			ROSConnection.GetOrCreateInstance().Subscribe<ImageMsg>(this.Topic, ImageMsgCallback);
		}

		private void ImageMsgCallback(ImageMsg obj)
		{
			
			int msgWidth = (int) obj.width;
			int msgHeight = (int) obj.height;
			var msgEncoding = RosImageEncodingEx.RosNameToEncoding[obj.encoding];
			
			if ((this.LastMsg != null) && (msgWidth != this.Width || msgHeight != this.Height || msgEncoding != this.Encoding))
				Debug.LogError($"ROS Image Message (width: {msgWidth}, height: {msgHeight}, encoding: {msgEncoding}) does not match previous received message(width: {this.Width}, height: {this.Height}, encoding: {this.Encoding}).");

			this.LastMsg = obj;
		}

		public void GetLastMessageAsTexture(Texture2D output)
		{
			if (this.LastMsg == null)
				return;

			if (this.Width != output.width || this.Height != output.height ||
			    this.Encoding.TextureFormat() != output.format)
				throw new ArgumentException($"Given texture does not match received images in size and encoding.");

			RosImageConverter.RosImageMessageToTexture(this.LastMsg, output);
		}

		public Texture2D GetLastMessageAsTexture()
		{
			var output = new Texture2D(this.Width, this.Height, this.Encoding.TextureFormat(), false);
			this.GetLastMessageAsTexture(output);
			return output;
		}
	}
}