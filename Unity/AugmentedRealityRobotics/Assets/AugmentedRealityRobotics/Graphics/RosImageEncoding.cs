using System.Collections.Generic;
using Shortbit.Utils;
using Shortbit.Utils.Enums;
using System.Linq;
using UnityEngine;

namespace ArrShared.Graphics
{
	public enum RosImageEncoding
	{
		[EnumStringValue("mono8")]
		[TextureFormatRepresentation(TextureFormat.R8, new []{ 0 })]
		
		Mono8,
		
		[EnumStringValue("rgb8")]
		[TextureFormatRepresentation(TextureFormat.RGB24, new []{ 0, 1, 2 })]
		RGB8,
		
		[EnumStringValue("bgr8")]
		[TextureFormatRepresentation(TextureFormat.RGB24, new []{ 2, 1, 0 })]
		BGR8,
		
		[EnumStringValue("rgba8")]
		[TextureFormatRepresentation(TextureFormat.RGBA32, new []{ 0, 1, 2, 3 })]
		RGBA8,
		
		[EnumStringValue("bgra8")]
		[TextureFormatRepresentation(TextureFormat.RGBA32, new []{ 2, 1, 0, 3 })]
		BGRA8,
		
		[EnumStringValue("mono16")]
		[TextureFormatRepresentation(TextureFormat.R16, new []{ 0 })]
		Mono16,
		
		[EnumStringValue("rgb16")]
		[TextureFormatRepresentation(TextureFormat.RGB48, new []{ 0, 1, 2 })]
		RGB16,
		
		[EnumStringValue("bgr16")]
		[TextureFormatRepresentation(TextureFormat.RGB48, new []{ 2, 1, 0 })]
		BGR16,
		
		[EnumStringValue("rgba16")]
		[TextureFormatRepresentation(TextureFormat.RGBA64, new []{ 0, 1, 2, 3 })]
		RGBA16,
		
		[EnumStringValue("bgra16")]
		[TextureFormatRepresentation(TextureFormat.RGBA64, new []{ 2, 1, 0, 3 })]
		BGRA16,
	}

	public static class RosImageEncodingEx
	{
		public static readonly IReadOnlyDictionary<RosImageEncoding, string> EncodingToRosName = EnumEx.LoadStringValues<RosImageEncoding>();

		public static readonly IReadOnlyDictionary<string, RosImageEncoding> RosNameToEncoding =
			EncodingToRosName.ToDictionary(p => p.Value, p => p.Key).AsReadOnly();


		public static readonly IReadOnlyDictionary<RosImageEncoding, TextureFormat> TextureFormatOfEncoding =
			EnumEx.LoadAttributeValues<RosImageEncoding, TextureFormatRepresentationAttribute, TextureFormat>(
				f => f.Format);
		
		public static readonly IReadOnlyDictionary<RosImageEncoding, int[]> ChannelOrderOfEncoding =
			EnumEx.LoadAttributeValues<RosImageEncoding, TextureFormatRepresentationAttribute, int[]>(
				f => f.RosToUnityChannelMap);

		public static string RosName(this RosImageEncoding self) => RosImageEncodingEx.EncodingToRosName[self];
		
		public static TextureFormat TextureFormat(this RosImageEncoding self) => RosImageEncodingEx.TextureFormatOfEncoding[self];
		public static int[] ChannelOrder(this RosImageEncoding self) => RosImageEncodingEx.ChannelOrderOfEncoding[self];
		
		public static int PixelByteSize(this RosImageEncoding self) => self.ChannelOrder().Length;
	}
}