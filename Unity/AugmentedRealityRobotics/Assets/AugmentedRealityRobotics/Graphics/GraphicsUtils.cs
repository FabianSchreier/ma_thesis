using System;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace ArrShared.Graphics
{
    public static class GraphicsUtils
    {
        public static RenderTexture CreateRandomAccessibleRenderTexture(Texture input)
        {
            /*
            var result = new RenderTexture(input.width, input.height, 0, input.graphicsFormat)
            {
                enableRandomWrite = true
            };
            result.Create();
            */

            var result = RenderTexture.GetTemporary(
                new RenderTextureDescriptor(input.width, input.height, input.graphicsFormat, 0, 0)
                {
                    enableRandomWrite = true,

                });
            return result;
        }

        public static void ReleaseGeneratedTexture(RenderTexture generated)
        {
            //generated.Release();
            RenderTexture.ReleaseTemporary(generated);
        }
        
        public static RenderTexture MakeRandomAccessibleCloneIfNecessary(RenderTexture input, bool force = false)
        {
            if (!force && input.enableRandomWrite)
                return input;

            return CreateRandomAccessibleRenderTexture(input);
        }

        public static void CopyFromGeneratedRandomAccessibleAndRelease(RenderTexture generated, RenderTexture original)
        {
            if (object.ReferenceEquals(generated, original)) return;
            
            UnityEngine.Graphics.Blit(generated, original);
            ReleaseGeneratedTexture(generated);
        }
        
        

        public static void RenderTextureToTexture2D(RenderTexture input, Texture2D output)
        {
            var activeRt = RenderTexture.active;

            RenderTexture.active = input;
            output.ReadPixels(new Rect(0, 0, Math.Min(input.width, output.width), Math.Min(input.height, output.height)), 0, 0);

            RenderTexture.active = activeRt;
        }

        public static Texture2D RenderTextureToTexture2D(RenderTexture input)
        {
            var output = new Texture2D(input.width, input.height,
                GraphicsFormatUtility.GetTextureFormat(input.graphicsFormat), true);
            RenderTextureToTexture2D(input, output);
            return output;
        }
        
    }
}