using System;
using RosMessageTypes.Sensor;
using Unity.Robotics.ROSTCPConnector;
using UnityEngine;

namespace ArrShared.Graphics
{
    public class RosCameraPublisher
    {
        public string Topic { get; }
        public string FrameId { get; }
        
        public RosImageEncoding Encoding { get; }
        
        public Texture2D Texture { get; }
        
        private readonly ROSConnection ros;

        private readonly CameraInfoMsg cameraInfoMessage;
        private ImageMsg imageMessage;
        
        
        public RosCameraPublisher(string topic, string frameId, RosImageEncoding encoding, float cameraFov, Texture2D texture)
        {
            this.Topic = topic;
            this.FrameId = frameId;
            this.Texture = texture;
            this.Encoding = encoding;
            
            this.ros = ROSConnection.GetOrCreateInstance();
            
            this.ros.RegisterPublisher<CameraInfoMsg>(this.Topic + "/camera_info");
            this.ros.RegisterPublisher<ImageMsg>(this.Topic + "/image_raw");
            
            var w = this.Texture.width;
            var h = this.Texture.height;
            var fov = cameraFov / 180 * Math.PI;

            var f = w / (2 * Math.Tan(fov / 2));

            this.cameraInfoMessage = new CameraInfoMsg
            {
                header = {frame_id = frameId}, 
                width = (uint) w, 
                height = (uint) h, 
                distortion_model = "plumb_bob",
                
                d = new[] {0.0, 0.0, 0.0, 0.0, 0.0},
                k = new[]
                {
                      f, 0.0, w/2.0+0.5, 
                    0.0,   f, h/2.0+0.5, 
                    0.0, 0.0, 1.0
                },
                r = new[]
                {
                    1.0, 0.0, 0.0, 
                    0.0, 1.0, 0.0, 
                    0.0, 0.0, 1.0
                },
                p = new[]
                {
                      f, 0.0, w/2.0+0.5, 0.0,
                    0.0,   f, h/2.0+0.5, 0.0,
                    0.0, 0.0, 1.0,       0.0
                }
            };
        }
        
        public void Publish()
        {
            if (this.imageMessage == null)
            {
                this.imageMessage = RosImageConverter.TextureToRosImageMessage(this.Texture, this.Encoding);
                this.imageMessage.header.frame_id = this.FrameId;
            }
            else
                RosImageConverter.TextureToRosImageMessage(this.Texture, this.Encoding, this.imageMessage);

            
            this.ros.Publish(this.Topic + "/camera_info", this.cameraInfoMessage);
            this.ros.Publish(this.Topic + "/image_raw", imageMessage);
            
        }
    }
}