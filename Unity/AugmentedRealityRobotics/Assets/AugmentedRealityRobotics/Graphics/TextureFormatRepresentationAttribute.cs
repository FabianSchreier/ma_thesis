using System;
using UnityEngine;

namespace ArrShared.Graphics
{
	public class TextureFormatRepresentationAttribute : Attribute
	{
		
		public TextureFormatRepresentationAttribute(TextureFormat format, int[] rosToUnityChannelMap)
		{
			this.Format = format;
			this.RosToUnityChannelMap = rosToUnityChannelMap;
		}
		
		/// <summary>
		/// The Unity TextureFormat used to represent this image encoding
		/// </summary>
		public TextureFormat Format { get; }
		
		/// <summary>
		/// An array of color channel indices.
		/// 
		/// Each element defines the ROS color index that the respective Unity channel (in RGBA order) should be taken from.
		/// E.g. with [2, 1, 0, 3] the first element means the first channel in Unity is the third channel in ROS.
		/// </summary>
		public int[] RosToUnityChannelMap { get; }
	}
}