using System;
using System.Collections;

namespace ArrShared
{
    public static class ThreadEx
    {

        public static void RunInMainThread(Action action)
        {
            if (UnityThreadHelper.MainThread == null)
                throw new InvalidOperationException("Unity main thread is not defined. Please call DefineMainThread() in a Start() or Update() function or create a GameObject containing the UnityThreadHelper component.");
            
            if (!UnityThreadHelper.IsMainThread)
            {
                var task = UnityThreadHelper.Dispatcher.Dispatch(action);
                task.Wait();
            }
            else
            {
                action();
            }
        }
        public static T RunInMainThread<T>(Func<T> action)
        {
            if (UnityThreadHelper.MainThread == null)
                throw new InvalidOperationException("Unity main thread is not defined. Please call DefineMainThread() in a Start() or Update() function or create a GameObject containing the UnityThreadHelper component.");
            
            if (!UnityThreadHelper.IsMainThread)
            {
                var task = UnityThreadHelper.Dispatcher.Dispatch(action);
                return task.Wait<T>();
            }
            else
            {
                return action();
            }
        }
        
    }
}