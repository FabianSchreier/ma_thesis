using System;
using RosMessageTypes.BuiltinInterfaces;

namespace ArrShared
{
    public static class TimeMsgEx
    {
        public static bool IsZero(this TimeMsg self)
        {
            return self.sec == 0 && self.nanosec == 0;
        }
        
        
        public static TimeMsg Now() => DateTimeOffset.UtcNow.ToTimeMsg();
        
        public static TimeMsg ToTimeMsg(this DateTimeOffset self)
        {
            var res = new TimeMsg()
            {
                sec = (uint)self.ToUnixTimeSeconds(),
                nanosec = (uint)self.Millisecond * 1000 * 1000
            };
            return res;
        }

        public static TimeMsg ToTimeMsg(this DateTime self) => new DateTimeOffset(self).ToTimeMsg();


        public static DateTimeOffset ToDateTimeOffset(this TimeMsg self)
        {
            var res = DateTimeOffset.FromUnixTimeSeconds(self.sec);
            res = res.AddMilliseconds(self.nanosec / 1000.0 / 1000.0);
            return res;
        }

        public static DateTime ToDateTime(this TimeMsg self)
        {
            var dto = self.ToDateTimeOffset();
            return dto.DateTime;
        }
    }
}