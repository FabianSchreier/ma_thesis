using RosMessageTypes.Geometry;
using Unity.Robotics.ROSTCPConnector.ROSGeometry;
using UnityEngine;

namespace ArrShared
{
    public static class CoordinateSpaceEx
    {

        public static void UpdateFromPose<C>(this Transform self, PoseMsg pose)
            where C : ICoordinateSpace, new()
        {
            self.position = pose.position.From<C>();
            self.rotation = pose.orientation.From<C>();
        }

        public static PoseMsg ToPose<C>(this Transform self)
            where C : ICoordinateSpace, new()
        {
            return new PoseMsg
            {
                position = self.position.To<C>(),
                orientation = self.rotation.To<C>()
            };
        }
        
    }
}