using ArrShared.Robot;
using ArrShared.Utils;
using RosMessages.Control.action;
using Shortbit.Utils;

namespace ArrShared
{
    public static class FollowJointTrajectoryEx
    {
        public static void UpdateFromJointState(this FollowJointTrajectoryFeedback self, ActuatingJointStateMap states )
        {
            self.joint_names = states.JointNames.ToArraySafe();
            
            ArrayEx.AssureLength(ref self.desired.positions, states.Count);
            ArrayEx.AssureLength(ref self.desired.velocities, states.Count);
            ArrayEx.AssureLength(ref self.desired.accelerations, states.Count);
            ArrayEx.AssureLength(ref self.desired.effort, states.Count);
            
            ArrayEx.AssureLength(ref self.actual.positions, states.Count);
            ArrayEx.AssureLength(ref self.actual.velocities, states.Count);
            ArrayEx.AssureLength(ref self.actual.accelerations, states.Count);
            ArrayEx.AssureLength(ref self.actual.effort, states.Count);
            
            ArrayEx.AssureLength(ref self.error.positions, states.Count);
            ArrayEx.AssureLength(ref self.error.velocities, states.Count);
            ArrayEx.AssureLength(ref self.error.accelerations, states.Count);
            ArrayEx.AssureLength(ref self.error.effort, states.Count);
            
            for (int i = 0; i < states.Count; i++)
            {
                var state = states[i];
                
                self.actual.positions[i] = state.Actual.Position;
                self.actual.velocities[i] = state.Actual.Velocity;
                self.actual.accelerations[i] = state.Actual.Acceleration;
            
                self.desired.positions[i] = state.Desired.Position;
                self.desired.velocities[i] = state.Desired.Velocity;
                self.desired.accelerations[i] = state.Desired.Acceleration;

                self.error.positions[i] = state.Error.Position;
                self.error.velocities[i] = state.Error.Velocity;
                self.error.accelerations[i] = state.Error.Acceleration;
            }
        }
    }
}