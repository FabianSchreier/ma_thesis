using System;
using ArrShared.Messages;
using RosMessageTypes.BuiltinInterfaces;

namespace ArrShared
{
    public static class DurationMsgEx
    {
        public static bool IsZero(this DurationMsg self)
        {
            return self.sec == 0 && self.nanosec == 0;
        }
        
        
        public static DurationMsg ToDurationMsg(this TimeSpan self)
        {
            var res = new DurationMsg()
            {
                sec = (int)Math.Floor(self.TotalSeconds),
                nanosec = (int)self.Milliseconds * 1000 * 1000
            };
            return res;
        }

        public static TimeSpan ToTimeSpan(this DurationMsg self)
        {
            return TimeSpan.FromSeconds(self.sec) + TimeSpan.FromMilliseconds(self.nanosec / 1000.0 / 1000.0);
        }

        public static DurationMsgFix Fix(this DurationMsg self)
        {
            return new DurationMsgFix()
            {
                sec = self.sec,
                nanosec = self.nanosec
            };
        }

    }
}