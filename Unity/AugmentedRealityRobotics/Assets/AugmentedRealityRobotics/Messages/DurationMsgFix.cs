using RosMessageTypes.BuiltinInterfaces;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;

namespace ArrShared.Messages
{
    public class DurationMsgFix : DurationMsg
    {
        public override void SerializeTo(MessageSerializer state)
        {
            state.Write(this.sec);
            state.Write(this.nanosec);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public new static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}