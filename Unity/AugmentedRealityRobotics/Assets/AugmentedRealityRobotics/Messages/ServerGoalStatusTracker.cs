using System;
using System.Threading;
using RosMessageTypes.Actionlib;
using RosMessageTypes.BuiltinInterfaces;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using Shortbit.Utils;

namespace ArrShared.Messages
{
    public class ServerGoalStatusTracker <TActionGoal, TGoal>
        where TActionGoal : ActionGoal<TGoal>
        where TGoal : Message
    {
        public TActionGoal Goal { get; }
        
        public GoalStatusMsg Status { get; }
        
        public DateTime? HandleDestructionTime { get; set; }
        
        public IGoalIDGenerator GoalIDGenerator { get; }
        
        public ServerGoalStatusTracker(GoalIDMsg goalId, GoalStatus status, IGoalIDGenerator goalIDGenerator = null)
        {
            this.GoalIDGenerator = goalIDGenerator ?? new UUIDGoalIDGenerator();
            
            Throw.IfNull(goalId, nameof(goalId));
            this.Status = new GoalStatusMsg(goalId, (byte)status, "");

        }
        
        public ServerGoalStatusTracker(TActionGoal goal, IGoalIDGenerator goalIDGenerator = null)
        {
            this.GoalIDGenerator = goalIDGenerator ?? new UUIDGoalIDGenerator();

            this.Goal = goal;

            this.Status = new GoalStatusMsg
            {
                goal_id = this.Goal.goal_id,
                status = (byte)GoalStatus.PENDING
            };

            if (string.IsNullOrEmpty(this.Status.goal_id.id))
                this.Status.goal_id.id = this.GoalIDGenerator.GenerateID();
            if (this.Status.goal_id.stamp.IsZero())
                this.Status.goal_id.stamp = TimeMsgEx.Now();


        }
    }
}