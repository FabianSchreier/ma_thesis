using System;
using RosMessageTypes.Actionlib;
using Unity.Collections;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using UnityEngine;

namespace ArrShared.Messages
{
    public class ServerGoalHandle<TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> : IEquatable<ServerGoalHandle<TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback>>
        where TActionGoal : ActionGoal<TGoal>, new()
        where TActionResult : ActionResult<TResult>, new()
        where TActionFeedback : ActionFeedback<TFeedback>, new()
        where TGoal : Message, new()
        where TResult : Message, new()
        where TFeedback : Message, new()
    {
        
        public ActionServer<TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> ActionServer
        {
            get;
        }
        
        public ServerGoalStatusTracker<TActionGoal, TGoal> StatusTracker { get; }

        public TActionGoal ActionGoal => this.StatusTracker.Goal;
        public TGoal Goal => this.ActionGoal.goal;
        
        public GoalStatusMsg GoalStatusMsg => this.StatusTracker.Status;

        public GoalIDMsg GoalID => this.StatusTracker.Status.goal_id;
        

        public ServerGoalHandle(ActionServer<TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> actionServer, 
                                ServerGoalStatusTracker<TActionGoal, TGoal> tracker)
        {
            this.ActionServer = actionServer;
            this.StatusTracker = tracker;
        }


        public bool Equals(ServerGoalHandle<TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return this.GoalID.id == other.GoalID.id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ServerGoalHandle<TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback>)obj);
        }

        public override int GetHashCode()
        {
            return this.GoalID.id.GetHashCode();
        }

        public void SetAccepted(string text = "")
        {
            lock (this.ActionServer.LockSync)
            {
                var status = (GoalStatus)this.StatusTracker.Status.status;
                
                // if we were pending before, then we'll go active
                if (status == GoalStatus.PENDING)
                {
                    this.StatusTracker.Status.status = (byte)GoalStatus.ACTIVE;
                    this.StatusTracker.Status.text = text;
                    this.ActionServer.PublishStatus();
                }
                // if we were recalling before, now we'll go to preempting
                else if (status == GoalStatus.RECALLING)
                {
                    this.StatusTracker.Status.status = (byte)GoalStatus.PREEMPTING;
                    this.StatusTracker.Status.text = text;
                    this.ActionServer.PublishStatus();
                }
                else
                    Debug.LogError($"To transition to an active state, the goal must be in a pending or recalling state, it is currently in state: {(GoalStatus)this.StatusTracker.Status.status}");
            }
        }

        public void SetCanceled(TResult result = null, string text = "")
        {
            result ??= new TResult();
            
            lock (this.ActionServer.LockSync)
            {
                var status = (GoalStatus)this.StatusTracker.Status.status;
                
                if (status == GoalStatus.PENDING || status == GoalStatus.RECALLING)
                {
                    this.StatusTracker.Status.status = (byte)GoalStatus.RECALLED;
                    this.StatusTracker.Status.text = text;
                    // on transition to a terminal state, we'll also set the handle destruction time
                    this.StatusTracker.HandleDestructionTime = DateTime.UtcNow;
                    this.ActionServer.PublishResult(this.StatusTracker.Status, result);
                }
                else if (status == GoalStatus.ACTIVE || status == GoalStatus.PREEMPTING)
                {
                    this.StatusTracker.Status.status = (byte)GoalStatus.PREEMPTED;
                    this.StatusTracker.Status.text = text;
                    // on transition to a terminal state, we'll also set the handle destruction time
                    this.StatusTracker.HandleDestructionTime = DateTime.UtcNow;
                    this.ActionServer.PublishResult(this.StatusTracker.Status, result);
                }
                else
                    Debug.LogError($"To transition to a cancelled state, the goal must be in a pending, recalling, active, or preempting state, it is currently in state: {(GoalStatus)this.StatusTracker.Status.status}");
            }
        }
        public void SetRejected(TResult result = null, string text = "")
        {
            result ??= new TResult();
            
            lock (this.ActionServer.LockSync)
            {
                var status = (GoalStatus)this.StatusTracker.Status.status;

                if (status == GoalStatus.PENDING || status == GoalStatus.RECALLING)
                {
                    this.StatusTracker.Status.status = (byte)GoalStatus.REJECTED;
                    this.StatusTracker.Status.text = text;
                    // on transition to a terminal state, we'll also set the handle destruction time
                    this.StatusTracker.HandleDestructionTime = DateTime.UtcNow;
                    this.ActionServer.PublishResult(this.StatusTracker.Status, result);
                }
                else
                    Debug.LogError($"To transition to a rejected state, the goal must be in a pending or recalling state, it is currently in state: {(GoalStatus)this.StatusTracker.Status.status}");
            }
        }
        public void SetAborted(TResult result = null, string text = "")
        {
            result ??= new TResult();
            
            lock (this.ActionServer.LockSync)
            {
                var status = (GoalStatus)this.StatusTracker.Status.status;

                if (status == GoalStatus.PREEMPTING || status == GoalStatus.ACTIVE)
                {
                    this.StatusTracker.Status.status = (byte)GoalStatus.ABORTED;
                    this.StatusTracker.Status.text = text;
                    // on transition to a terminal state, we'll also set the handle destruction time
                    this.StatusTracker.HandleDestructionTime = DateTime.UtcNow;
                    this.ActionServer.PublishResult(this.StatusTracker.Status, result);
                }
                else
                    Debug.LogError($"To transition to a aborted state, the goal must be in a preempting or active state, it is currently in state: {(GoalStatus)this.StatusTracker.Status.status}");
            }
        }
        public void SetSucceeded(TResult result = null, string text = "")
        {
            result ??= new TResult();
            
            lock (this.ActionServer.LockSync)
            {
                var status = (GoalStatus)this.StatusTracker.Status.status;

                if (status == GoalStatus.PREEMPTING || status == GoalStatus.ACTIVE)
                {
                    this.StatusTracker.Status.status = (byte)GoalStatus.SUCCEEDED;
                    this.StatusTracker.Status.text = text;
                    // on transition to a terminal state, we'll also set the handle destruction time
                    this.StatusTracker.HandleDestructionTime = DateTime.UtcNow;
                    this.ActionServer.PublishResult(this.StatusTracker.Status, result);
                }
                else
                    Debug.LogError($"To transition to a succeeded state, the goal must be in a preempting or active state, it is currently in state: {(GoalStatus)this.StatusTracker.Status.status}");
            }
        }
        
        internal bool SetCancelRequested()
        {
            lock (this.ActionServer.LockSync)
            {
                var status = (GoalStatus)this.StatusTracker.Status.status;

                if (status == GoalStatus.PENDING)
                {
                    this.StatusTracker.Status.status = (byte)GoalStatus.RECALLING;
                    this.ActionServer.PublishStatus();
                    return true;
                }
                
                if (status == GoalStatus.ACTIVE)
                {
                    this.StatusTracker.Status.status = (byte)GoalStatus.PREEMPTING;
                    this.ActionServer.PublishStatus();
                    return true;
                }
                
            }

            return false;
        }

        public void PublishFeedback(TFeedback feedback)
        {
            lock (this.ActionServer.LockSync)
            {
                this.ActionServer.PublishFeedback(this.StatusTracker.Status, feedback);
            }
        }
    }
}