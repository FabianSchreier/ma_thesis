namespace ArrShared.Messages
{
    public interface IGoalIDGenerator
    {
        public string GenerateID();
    }
}