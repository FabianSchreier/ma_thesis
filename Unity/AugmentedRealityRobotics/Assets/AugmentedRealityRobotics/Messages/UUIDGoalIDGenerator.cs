using Unity.Robotics.ROSTCPConnector;
using UnityEditor;

namespace ArrShared.Messages
{
    public class UUIDGoalIDGenerator : IGoalIDGenerator
    {

        public string GenerateID()
        {
            var uuid = GUID.Generate();
            return uuid.ToString();
        }
        
    }
}