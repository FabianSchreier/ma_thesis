using System;
using System.Collections.Generic;
using System.Threading;
using RosMessageTypes.Actionlib;
using Shortbit.Utils;
using Unity.Robotics.ROSTCPConnector;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using UnityEngine;
using UnityThreading;

namespace ArrShared.Messages
{
    
    public class ActionServer<TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> : IDisposable
        where TActionGoal : ActionGoal<TGoal>, new()
        where TActionResult : ActionResult<TResult>, new()
        where TActionFeedback : ActionFeedback<TFeedback>, new()
        where TGoal : Message, new()
        where TResult : Message, new()
        where TFeedback : Message, new()
    {


        public string Namespace { get; }
        
        public IGoalIDGenerator GoalIDGenerator { get; set; }
        
        public bool Initialized { get; private set; }
        public bool Started { get; private set; }

        private readonly ROSConnection ros;
        
        private readonly Action<ServerGoalHandle<TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback>> goalCallback;
        private readonly Action<ServerGoalHandle<TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback>> cancelCallback;

        public readonly object LockSync = new object();

        private readonly string statusTopic;
        private readonly string resultTopic;
        private readonly string feedbackTopic;
        private readonly string goalTopic;
        private readonly string cancelTopic;

        private readonly List<ServerGoalStatusTracker<TActionGoal, TGoal>> trackedGoals = new List<ServerGoalStatusTracker<TActionGoal, TGoal>>();

        private DateTime lastCancel;

        private readonly TimeSpan trackingTimeout;

        private Rate statusUpdateRate;
        //private readonly ManualResetEvent shutdownEvent = new ManualResetEvent(false);
        //private readonly Thread statusUpdateThread;
        
        public ActionServer(string ns, 
            Action<ServerGoalHandle<TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback>> goalCallback,
            Action<ServerGoalHandle<TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback>> cancelCallback = null,
            ROSConnection ros = null,
            Rate statusUpdateRate = null)
        {
            this.Namespace = Throw.IfNullOrEmpty(ns, nameof(ns));
            this.goalCallback = Throw.IfNull(goalCallback, nameof(goalCallback));
            this.cancelCallback = cancelCallback;

            this.statusTopic = this.Namespace + "/status";
            this.resultTopic = this.Namespace + "/result";
            this.feedbackTopic = this.Namespace + "/feedback";
            this.goalTopic = this.Namespace + "/goal";
            this.cancelTopic = this.Namespace + "/cancel";

            this.ros = ros ? ros : ROSConnection.GetOrCreateInstance();

            this.GoalIDGenerator = new UUIDGoalIDGenerator();

            //this.statusUpdateThread = new Thread(this.StatusUpdateThreadRun);
            this.statusUpdateRate = statusUpdateRate ?? new Rate(5);

            this.lastCancel = DateTime.MinValue;
            this.trackingTimeout = TimeSpan.FromSeconds(5);

            this.Initialized = false;
            this.Started = false;
            
        }

        public void Start()
        {
            if (this.Started)
                return;

            lock (this.LockSync)
            {
                if (this.Started)
                    return;
                
                this.Initialize();
                this.Started = true;
                this.PublishStatus();
            }
        }

        public void Initialize()
        {
            if (this.Initialized)
                return;
            
            lock (this.LockSync)
            {
                if (this.Initialized)
                    return;

                this.Initialized = true;
                

                this.ros.Subscribe<TActionGoal>(this.goalTopic, this.GoalSubscriberCallback);
                this.ros.Subscribe<GoalIDMsg>(this.cancelTopic, this.CancelSubscriberCallback);
                ros.RegisterPublisher<TActionFeedback>(this.feedbackTopic);
                ros.RegisterPublisher<TActionResult>(this.resultTopic);
                ros.RegisterPublisher<GoalStatusArrayMsg>(this.statusTopic);
                
                //this.statusUpdateThread.Start();
            }
        }

        public void PublishStatus()
        {
            lock (this.LockSync)
            {

                var removeItems = new List<ServerGoalStatusTracker<TActionGoal, TGoal>>();
                var statusList = new List<GoalStatusMsg>();
                foreach (var tracker in this.trackedGoals)
                {
                    if (tracker.HandleDestructionTime.HasValue &&
                        tracker.HandleDestructionTime + this.trackingTimeout < DateTime.UtcNow)
                        removeItems.Add(tracker);
                    else
                        statusList.Add(tracker.Status);
                }
                this.trackedGoals.RemoveAll(removeItems.Contains);

                var statusArrayMsg = new GoalStatusArrayMsg
                {
                    status_list = statusList.ToArray(),
                    header =
                    {
                        stamp = TimeMsgEx.Now()
                    }
                };
                this.ros.Publish(this.statusTopic, statusArrayMsg);
                //ThreadEx.RunInMainThread(() => this.ros.Publish(this.statusTopic, statusArrayMsg));


            }
        }

        public void PublishResult(GoalStatusMsg status, TResult result)
        {
            lock (this.LockSync)
            {
                var ar = new TActionResult
                {
                    header =
                    {
                        stamp = TimeMsgEx.Now()
                    },
                    status = status,
                    result = result
                };
                this.ros.Publish(this.resultTopic, ar);
                //ThreadEx.RunInMainThread(() => this.ros.Publish(this.resultTopic, ar));
                this.PublishStatus();

            }
            
        }

        public void PublishFeedback(GoalStatusMsg status, TFeedback feedback)
        {
            lock (this.LockSync)
            {
                var af = new TActionFeedback
                {
                    header =
                    {
                        stamp = TimeMsgEx.Now()
                    },
                    status = status,
                    feedback = feedback
                };
                this.ros.Publish(this.feedbackTopic, af);
                //ThreadEx.RunInMainThread(() => this.ros.Publish(this.feedbackTopic, af));

            }
        }


        public void Spin()
        {
            this.SpinStatusUpdate();
        }

        private void SpinStatusUpdate()
        {
            Rate.AssureRateInstance(ref this.statusUpdateRate, 5);
            if (!this.Started || !this.statusUpdateRate.Tick())
                return;
            this.PublishStatus();

        }
        /*
        private void StatusUpdateThreadRun()
        {
            while (!this.shutdownEvent.WaitOne(100))
            {
                if(!this.Started)
                    continue;
                this.PublishStatus();
            }
        }
        */

        private void GoalSubscriberCallback(TActionGoal goal)
        {
            lock (this.LockSync)
            {
                if (!this.Started)
                    return;
                
                Debug.Log($"Received goal {goal.goal_id.id}");

                // we need to check if this goal already lives in the status list
                foreach (var st in this.trackedGoals)
                {
                    if (goal.goal_id.id != st.Status.goal_id.id)
                        continue;
    
                    // Goal could already be in recalling state if a cancel came in before the goal
                    if ((GoalStatus)st.Status.status == GoalStatus.RECALLING)
                    {
                        st.Status.status = (byte)GoalStatus.RECALLED;
                        this.PublishResult(st.Status, new TResult());
                    }
                    
                    // make sure not to call any user callbacks or add duplicate status onto the list
                    return;
                }
                
                // if the goal is not in our list, we need to create a StatusTracker associated with this goal and push it on
                var tracker = new ServerGoalStatusTracker<TActionGoal, TGoal>(goal, this.GoalIDGenerator);
                this.trackedGoals.Add(tracker);

                var gh =
                    new ServerGoalHandle<TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback>(
                        this, tracker);
                
                // check if this goal has already been canceled based on its timestamp
                if (!goal.goal_id.stamp.IsZero() && goal.goal_id.stamp.ToDateTime() < this.lastCancel)
                    gh.SetCanceled(
                        text:
                        "This goal handle was canceled by the action server because its timestamp is before the timestamp of the last cancel request");
                else
                    this.goalCallback(gh);
            }
        }

        private void CancelSubscriberCallback(GoalIDMsg goalID)
        {
            lock (this.LockSync)
            {
                if (!this.Started)
                    return;

                var goalIDFound = false;
                foreach (var st in this.trackedGoals)
                {
                    
                    var cancelEverything = (goalID.id == "" && goalID.stamp.IsZero()); // id and stamp 0 --> cancel everything
                    var cancelThisOne = (goalID.id == st.Status.goal_id.id);  // ids match... cancel that goal
                    var cancelBeforeStamp = (!goalID.stamp.IsZero() && st.Status.goal_id.stamp.ToDateTime() <= goalID.stamp.ToDateTime()); // stamp != 0 --> cancel everything before stamp

                    if (!cancelEverything && !cancelThisOne && !cancelBeforeStamp)
                        continue;

                    if (goalID.id == st.Status.goal_id.id)
                        goalIDFound = true;
                    
                    st.HandleDestructionTime ??= DateTime.UtcNow;

                    var gh =
                        new ServerGoalHandle<TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback>(
                            this, st);

                    if (gh.SetCancelRequested())
                        this.cancelCallback(gh);
                }

                // if the requested goal_id was not found, and it is non-zero, then we need to store the cancel request
                if (goalID.id != "" && !goalIDFound)
                {
                    var tracker = new ServerGoalStatusTracker<TActionGoal, TGoal>(goalID, GoalStatus.RECALLING);
                    this.trackedGoals.Add(tracker);
                    tracker.HandleDestructionTime = DateTime.UtcNow;
                }
                

                
                if (goalID.stamp.ToDateTime() > this.lastCancel)
                {
                    this.lastCancel = goalID.stamp.ToDateTime();
                }
            }
        }

        
        public void Dispose()
        {
            /*
            this.shutdownEvent.Set();
            this.statusUpdateThread.Join();

            shutdownEvent?.Dispose();
            */
        }
    }
}