using UnityEngine;

namespace ArrShared
{
	public class Rate
	{
		public static void AssureRateInstance(ref Rate instance, int frequency)
		{
			if (instance == null)
				instance = new Rate(frequency);
			else if (instance.Frequency != frequency)
				instance.Frequency = frequency;
				
		}
		
		private int frequency;
		private double timeDelta;

		private double lastTick;
		
		public int Frequency
		{
			get => this.frequency;
			set
			{
				this.frequency = value;
				this.timeDelta = 1 / (double)this.frequency;
				
			}
		}
		
		public Rate(int frequency)
		{
			this.Frequency = frequency;
		}
		
		

		public bool Tick()
		{
			var shouldUpdate = (Time.fixedTime - this.lastTick) > this.timeDelta;

			if (shouldUpdate)
				this.lastTick = Time.fixedTime;
			return shouldUpdate;
		}
	}
}