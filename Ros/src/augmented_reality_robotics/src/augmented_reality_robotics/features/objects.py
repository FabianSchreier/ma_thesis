# Imports


__author__ = 'Fabian Schreier'
__version__ = "0.1.0"
__status__ = "Prototype"

import contextlib
import threading
from collections import defaultdict
from copy import deepcopy

import numpy as np
import rospy
from typing import Dict, Optional, Iterator, ContextManager, AbstractSet, ValuesView, ItemsView, Mapping

import augmented_reality_robotics.msg as msgs
import augmented_reality_robotics.srv as srvs
from robot_franka_task_envs import utils
from robot_franka_task_envs.utils import R


class ObjectState:
    def __init__(self, feature: 'ObjectStateFeature', message: msgs.ObjectState, last_update: rospy.Time, original: 'ObjectState' = None):
        self.feature = feature
        self.message = message
        self.last_update = last_update
        self._original = original

        self._orphaned = False

    def _raise_if_orphaned(self):
        if self._original is not None:
            self._original._raise_if_orphaned()
        elif self._orphaned:
            raise ValueError('Can\'t access an _orphaned object state.')

    def mark_orphaned(self):
        if self._original is not None:
            self._original.mark_orphaned()
        else:
            self._orphaned = True

    def copy(self) -> 'ObjectState':
        self._raise_if_orphaned()

        return ObjectState(
            feature=self.feature,
            message=deepcopy(self.message),
            last_update=deepcopy(self.last_update),
            original=self._original if self._original is not None else self
        )

    @contextlib.contextmanager
    def update(self) -> ContextManager['ObjectState']:
        clone = self.copy()
        yield clone
        self.feature.update_object_state(clone)

    @property
    def object_id(self) -> str:
        self._raise_if_orphaned()
        return self.message.object_id

    @property
    def position(self) -> np.ndarray:
        self._raise_if_orphaned()
        return utils.point_msg_to_vector(self.message.pose.position)

    @position.setter
    def position(self, value: np.ndarray):
        self._raise_if_orphaned()
        self.message.pose.position = utils.vector_to_point_msg(value)

    @property
    def orientation(self) -> R:
        self._raise_if_orphaned()
        return R.from_quat_msg(self.message.pose.orientation)

    @orientation.setter
    def orientation(self, value: R):
        self._raise_if_orphaned()
        self.message.pose.orientation = value.as_quat_msg()


class ObjectStateFeature(Mapping[str, ObjectState]):

    def __init__(self, state_topic='/augmented_environment/objects/state', update_topic='/augmented_environment/objects/update'):
        self.state_subscriber = rospy.Subscriber(state_topic, msgs.ObjectStateArrayStamped, self._state_subscriber_callback, queue_size=1)
        self.update_service = rospy.ServiceProxy(update_topic, srvs.UpdateObjectState)

        self._tracked_objects: Dict[str, ObjectState] = dict()
        self._pending_update_calls: Dict[str, threading.Event] = dict()

    def _state_subscriber_callback(self, state_msg: msgs.ObjectStateArrayStamped):

        state: msgs.ObjectState
        for state in state_msg.object_state_array.objects:
            obj = self._tracked_objects.get(state.object_id)
            if obj is None:
                obj = ObjectState(self, state, rospy.Time.now())
                self._tracked_objects[state.object_id] = obj
            else:
                obj.message = state
                obj.last_update = rospy.Time.now()

            if state.object_id in self._pending_update_calls:
                self._pending_update_calls[state.object_id].set()

    def spin(self):
        pass

    def update_object_state(self, object_state: ObjectState):
        result: srvs.UpdateObjectStateResponse = self.update_service.call(object_state.message)
        if not result.successful:
            raise ValueError(result.error_message)

    def wait_for_state(self, object_id: str) -> ObjectState:
        own_ev = threading.Event()
        ev = self._pending_update_calls.setdefault(object_id, own_ev)

        ev.wait()
        obj = self[object_id]
        if ev is own_ev:  # Only clear dict entry if it was _our_ event that got used (and thus inserted)
            del self._pending_update_calls[object_id]

        return obj


    def get(self, object_id: str) -> Optional[ObjectState]:
        return self._tracked_objects.get(object_id)

    def __getitem__(self, object_id: str) -> ObjectState:
        return self._tracked_objects[object_id]

    def __len__(self) -> int:
        return len(self._tracked_objects)

    def __iter__(self) -> Iterator[ObjectState]:
        return iter(self._tracked_objects.values())

    def keys(self) -> AbstractSet[str]:
        return self._tracked_objects.keys()

    def values(self) -> ValuesView[ObjectState]:
        return self._tracked_objects.values()

    def items(self) -> ItemsView[str, ObjectState]:
        return self._tracked_objects.items()




