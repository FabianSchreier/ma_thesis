from robot_common_task_tools.env_wrappers.exception_handling import EnvironmentExecutionException
from robot_franka_task_envs.features.robots import RobotMovementErrorState


class RobotMovementFailedException(EnvironmentExecutionException):
    def __init__(self, msg: str, state: RobotMovementErrorState):
        super().__init__(msg)
        self.state = state

