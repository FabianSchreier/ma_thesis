from copy import deepcopy

import rospy
from control_msgs.msg import JointTolerance, FollowJointTrajectoryGoal
from typing import Dict, Any

from robot_franka_task_envs.utils import isclose


class JointTolerances:
    def __init__(self, goal: float, trajectory: float):
        self.goal = goal
        self.trajectory = trajectory


class Tolerances:
    def __init__(self, params: Dict[str, Any]):
        self.goal_time = rospy.Duration.from_sec(params.get('goal_time', 0.0))
        self.apply_on_backend: bool = params.get('apply_on_backend', False)

        self.joints: Dict[str, JointTolerances] = {}

        for k, v in params.get('joints').items():
            self.joints[k] = JointTolerances(v.get('goal', 0.0), v.get('trajectory', 0.0))

    def clone_and_apply(self, goal: FollowJointTrajectoryGoal) -> FollowJointTrajectoryGoal:
        goal = deepcopy(goal)
        self.apply(goal)
        return goal

    def apply(self, goal: FollowJointTrajectoryGoal):
        if goal.goal_time_tolerance == rospy.Duration():
            goal.goal_time_tolerance = self.goal_time

        missing_joints = set(self.joints.keys())

        tolerance: JointTolerance
        for tolerance in goal.goal_tolerance:
            joint_tolerance = self.joints.get(tolerance.name)
            if joint_tolerance is None:
                continue
            missing_joints.remove(tolerance.name)

            tolerance.position = self._apply_value(tolerance.position, joint_tolerance.goal)

        for joint_name in missing_joints:
            joint_tolerance = self.joints.get(joint_name)
            goal.goal_tolerance.append(JointTolerance(
                name=joint_name,
                position=joint_tolerance.goal
            ))

        missing_joints = set(self.joints.keys())
        for tolerance in goal.path_tolerance:
            joint_tolerance = self.joints.get(tolerance.name)
            if joint_tolerance is None:
                continue
            missing_joints.remove(tolerance.name)

            tolerance.position = self._apply_value(tolerance.position, joint_tolerance.trajectory)

        for joint_name in missing_joints:
            joint_tolerance = self.joints.get(joint_name)
            goal.path_tolerance.append(JointTolerance(
                name=joint_name,
                position=joint_tolerance.trajectory
            ))

    def _apply_value(self, value: float, default_value: float) -> float:
        if isclose(value, 0.0):
            return default_value
        elif value < 0:
            return 0.0
        return value
