# Imports


__author__ = 'Fabian Schreier'
__version__ = "0.1.0"
__status__ = "Prototype"

import math
from enum import unique, Enum, auto
from typing import TypedDict, Tuple, Callable, Union, Optional, cast, List, Iterable, Dict

import numpy as np
import gym
import rospy
import std_msgs.msg
from gym.utils.seeding import np_random

from augmented_reality_robotics.exceptions import RobotMovementFailedException
from augmented_reality_robotics.features.objects import ObjectStateFeature
from robot_common_task_tools.env_wrappers import WrapperDef, EnvironmentId, apply_env_id_wrapper_combination, \
    apply_env_wrapper_combination, get_all_wrapper_combinations
from robot_common_task_tools.env_wrappers.exception_handling import ExceptionHandlingEnvMixin, \
    EnvironmentExecutionException
from robot_common_task_tools.env_wrappers.generic import ScaledRewardEnv
from robot_common_task_tools.utils.gym import GoalEnvObservationSpace, gym_register_function_wrapper, \
    tune_register_function_wrapper
from robot_franka_task_envs.features.robots import RobotMovementSuccessState, RobotMovementErrorState
from robot_franka_task_envs.features.robots.generic import EndEffectorTransformationFeature
from robot_franka_task_envs.utils import R, gym_space_matches
from robot_common_task_tools.extensions.rendering_mixin import RenderingMixin
from robot_franka_task_envs.features.robots.panda import PandaMoveItFeature, panda_default_ee_transformation, \
    FrankaGripperPandaFingerFeature


@unique
class AugmentedReachingEnvImageObservation(Enum):
    NoImages = 1
    #BothImages = 2


@unique
class AugmentedReachingEnvActionType(Enum):
    RelativeEndEffector = 1


@unique
class AugmentedReachingEnvRewardMode(Enum):
    """
    Possible rewards for the environment:
    * SparseSuccessOnly: Returns 1 if the environment is in a success state, else 0
    * NegativeL2Norm: returns -np.linalg.norm(achieved_goal-desired_goal)
    """
    SparseSuccessOnly = auto()
    NegativeL2Norm = auto()


class AugmentedReachingEnvInfoDict(TypedDict):
    is_success: bool
    violated_ee_bounds: bool
    movement_failed: bool


class AugmentedReachingEnvExtraArguments(TypedDict, total=False):
    image_height: int
    image_width: int

    max_episode_length: int
    maximum_time_penalty: float

    linear_action_size: float

    ros_endpoint: Tuple[str, int]


class AugmentedReachingEnv(gym.GoalEnv, RenderingMixin, ExceptionHandlingEnvMixin):

    def __init__(self,
                 *,
                 image_observation: AugmentedReachingEnvImageObservation,
                 action_type: AugmentedReachingEnvActionType,
                 starting_setup_function: Callable[[np.random.RandomState], np.ndarray],
                 goal_function: Callable[[np.array, np.random.RandomState], np.array],
                 goal_position_space: gym.spaces.Space,
                 reward_mode: AugmentedReachingEnvRewardMode,
                 success_distance: float = 0.01,
                 success_bonus: float,
                 terminate_on_success: bool = True,
                 allowed_ee_bounds: gym.spaces.Box,
                 ee_bound_violation_penalty: float,
                 terminate_on_ee_bound_violation: bool = True,
                 terminate_on_box_bound_violation: bool = True,
                 time_penalty: float,

                 max_action_offset: Union[float, np.ndarray],
                 finite_time: bool = True,
                 max_episode_length: int = 50,
                 observation_image_width: int,
                 observation_image_height: int,

                 motorized_joints: int = 7):

        self.image_observation = image_observation
        self.observation_image_width = observation_image_width
        self.observation_image_height = observation_image_height
        self.action_type = action_type
        self.starting_setup_function = starting_setup_function
        self.goal_function = goal_function
        self.goal_position_space = goal_position_space
        self.reward_mode = reward_mode
        self.success_distance = success_distance
        self.success_bonus = success_bonus
        self.terminate_on_success = terminate_on_success
        self.allowed_ee_bounds = allowed_ee_bounds
        self.ee_bound_violation_penalty = ee_bound_violation_penalty
        self.terminate_on_ee_bound_violation = terminate_on_ee_bound_violation
        self.terminate_on_box_bound_violation = terminate_on_box_bound_violation
        self.max_action_offset = max_action_offset
        self.finite_time = finite_time
        self.time_penalty = time_penalty
        self.max_episode_length = max_episode_length

        self.observation_image_width = observation_image_width
        self.observation_image_height = observation_image_height

        self.num_motorized_joints = motorized_joints

        self.default_orientation = R.about_axis(math.pi, (1, 0, 0))
        self.starting_box_height = 0.05

        assert self.max_episode_length > 0 or not self.finite_time
        assert self.ee_bound_violation_penalty >= 0

        assert cast(np.ndarray, self.allowed_ee_bounds.low < self.allowed_ee_bounds.high).all()
        assert self.allowed_ee_bounds.shape == (3,)

        assert self.action_type is not None

        self.action_space = self._build_action_space()
        self.observation_space = self._build_observation_space()
        self.goal_position_space = self._build_goal_space()
        self.steps = 0

        self.robot_control_raw = PandaMoveItFeature(acceleration_scaling_factor=0.5, velocity_scaling_factor=0.5)
        self.robot_control = EndEffectorTransformationFeature(self.robot_control_raw, panda_default_ee_transformation())
        self.object_states = ObjectStateFeature()

        self.random_generator: Optional[np.random.RandomState] = None
        self.seed()

        self.goal: Optional[np.array] = None

        try:
            rospy.init_node('augmented_environments', anonymous=True)
        except:
            pass

    def _build_goal_space(self) -> gym.spaces.Dict:
        return gym.spaces.Dict({
            "end_effector_xyz": self.goal_position_space
        })

    def _build_observation_space(self) -> gym.spaces.Dict:
        observation_part = {
            "used_time": gym.spaces.Box(dtype=np.float32, shape=(1,), low=0., high=1.),
            "end_effector_xyz": gym.spaces.Box(dtype=np.float32, shape=(3,),
                                               low=-1.5 * np.ones((3,)),
                                               high=1.5 * np.ones((3,))),
            "end_effector_orientation": gym.spaces.Box(dtype=np.float32, shape=(4,),
                                                       low=-np.inf,
                                                       high=np.inf),
        }
        '''
        if self.image_observation is AugmentedReachingEnvImageObservation.BothImages:
            observation_part["rgb_image"] = \
                gym.spaces.Box(dtype=np.uint8,
                               shape=(self.observation_image_height, self.observation_image_width, 3),
                               low=0,
                               high=255)

            observation_part["depth_image"] = \
                gym.spaces.Box(dtype=np.uint8,
                               shape=(self.observation_image_height, self.observation_image_width),
                               low=0,
                               high=255)
        '''
        achieved_goal = {
            "end_effector_xyz": gym.spaces.Box(dtype=np.float32, shape=(3,),
                                               low=-1.5 * np.ones((3,)),
                                               high=1.5 * np.ones((3,)))
        }
        desired_goal = {
            "end_effector_xyz": gym.spaces.Box(dtype=np.float32, shape=(3,),
                                               low=-1.5 * np.ones((3,)),
                                               high=1.5 * np.ones((3,)))
        }
        #achieved_goal = self._build_goal_space()
        #desired_goal = self._build_goal_space()

        return gym.spaces.Dict({
            "observation": gym.spaces.Dict(observation_part),
            "achieved_goal": gym.spaces.Dict(achieved_goal),
            "desired_goal": gym.spaces.Dict(desired_goal)
        })

    def _build_action_space(self) -> gym.spaces.Box:
        if self.action_type is AugmentedReachingEnvActionType.RelativeEndEffector:
            return gym.spaces.Box(dtype=np.float32,
                                  shape=(3,),
                                  low=-np.ones((3,)),
                                  high=np.ones((3,)))

    def handle_environment_exception(self, exception: EnvironmentExecutionException):
        assert isinstance(exception, RobotMovementFailedException)
        print('Robot movement failed. Exception message: %s' % str(exception))
        print('Please reset the robot and confirm')

        rospy.wait_for_message('/augmented_environment/reset/confirm', std_msgs.msg.Empty)
        print('Reset confirmed')

    def _render(self, width, height):
        return np.zeros(shape=(self.observation_image_height, self.observation_image_width, 3), dtype=np.float32), np.zeros(shape=(self.observation_image_height, self.observation_image_width), dtype=np.float32)
        #raise NotImplementedError()

    def _robot_position(self) -> np.ndarray:
        return self.robot_control.get_current_position().astype(np.float32)

    def _observe(self) -> GoalEnvObservationSpace:
        used_time = 0. if not self.finite_time else (self.steps / float(self.max_episode_length))

        robot_position = self._robot_position()

        observation_part = {
            "used_time": np.asarray([used_time], dtype=np.float32),
            "end_effector_xyz": robot_position,
            "end_effector_orientation": self.robot_control.get_current_orientation().as_quat().astype(np.float32),
        }

        # TODO: Add image rendering
        # rgb, depth = self._render(self.observation_image_width, self.observation_image_height)
        # observation_part["rgb_image"] = rgb
        # observation_part["depth_image"] = depth

        achieved_goal = {
            "end_effector_xyz": robot_position
        }
        desired_goal = {
            "end_effector_xyz": self.goal
        }

        obs = {
            "observation": observation_part,
            "achieved_goal": achieved_goal,
            "desired_goal": desired_goal
        }
        matches = gym_space_matches(self.observation_space, obs)
        assert self.observation_space.contains(obs), "Observation space does not contain given observation.\nMatches: %s" % matches
        return obs

    def reset(self) -> GoalEnvObservationSpace:
        super().reset()

        movement_state = self.robot_control.move_to_reset_pose()
        if not movement_state.is_success:
            assert isinstance(movement_state, RobotMovementErrorState)
            raise RobotMovementFailedException("Could not move to reset pose (position test reset). state: %s" % movement_state, movement_state)

        starting_robot_position = self._generate_start_position()

        movement_state = self.robot_control.move_to_reset_pose()
        if not movement_state.is_success:
            assert isinstance(movement_state, RobotMovementErrorState)
            raise RobotMovementFailedException("Could not move to reset pose (action start reset). state: %s" % movement_state,
                                                movement_state)

        movement_state = self.robot_control.move_to_position_and_orientation(starting_robot_position, self.default_orientation)

        if not movement_state.is_success:
            assert isinstance(movement_state, RobotMovementErrorState)
            raise RobotMovementFailedException("Could not move to robot start pose. state: %s" % movement_state, movement_state)

        self.goal = self._generate_goal_position(starting_robot_position)

        self.steps = 0

        obs = self._observe()
        matches = gym_space_matches(self.goal_position_space, obs['desired_goal'])
        assert self.goal_position_space.contains(obs["desired_goal"])
        return obs

    def _extend_2d_box_position(self, position: np.ndarray) -> np.ndarray:
        return np.asarray([position[0], position[1], self.starting_box_height])

    def _generate_start_position(self):
        reachable = False
        iterations = 0
        starting_robot_position = None
        while not reachable:
            starting_robot_position = self.starting_setup_function(self.random_generator)

            if len(starting_robot_position) != 3:
                raise ValueError(
                    "Starting setup function returned array of wrong dimension. No matter what action type is,"
                    "the starting function must always return a 3D world position vector for the starting"
                    " robot position")

            movement_status = self.robot_control.move_to_position_and_orientation(starting_robot_position, self.default_orientation)
            if movement_status.is_success:
                actual_robot_position = self.robot_control.get_current_position()
                reachable = np.linalg.norm(actual_robot_position - starting_robot_position) < 0.01
            else:
                reachable = False

            if iterations > 25:
                raise Exception('Could not generate a valid robot and box start position after %d iterations' % 25)
        return starting_robot_position.astype(np.float32)

    def _generate_goal_position(self, start_position):
        reachable = False
        iterations = 0
        goal_robot_position = None
        while not reachable:
            goal_robot_position = self.goal_function(start_position, self.random_generator)

            if len(goal_robot_position) != 3:
                raise ValueError(
                    "goal setup function returned array of wrong dimension. No matter what action type is,"
                    "the goal function must always return a 3D world position vector for the goal"
                    " robot position")

            movement_status = self.robot_control.move_to_position_and_orientation(goal_robot_position, self.default_orientation)
            if movement_status.is_success:
                actual_robot_position = self.robot_control.get_current_position()
                reachable = np.linalg.norm(actual_robot_position - goal_robot_position) < 0.01
            else:
                reachable = False

            if iterations > 25:
                raise Exception('Could not generate a valid robot and box start position after %d iterations' % 25)
        return goal_robot_position.astype(np.float32)

    def seed(self, seed=None):
        self.action_space.seed(seed)
        self.random_generator, reproducer_seed = np_random(seed)

        return [reproducer_seed]

    def _execute_action(self, action: np.ndarray) -> bool:
        assert action.shape == self.action_space.shape

        # We no longer check if the action is correct, but rather make it so!
        # RLLib (and apparently gym, too) do not enforce correct action space.
        #assert self.action_space.contains(action)
        action = np.clip(action, self.action_space.low, self.action_space.high).astype(self.action_space.dtype)

        ee_position_delta = action[:3]
        current_ee_xyz = self._robot_position()

        scaled_action = np.multiply(ee_position_delta, self.max_action_offset)
        action_xyz = scaled_action

        new_xyz: np.ndarray = (current_ee_xyz + action_xyz).astype(np.float32)

        if self.allowed_ee_bounds.contains(new_xyz):
            movement_state = self.robot_control.move_to_position_and_orientation(new_xyz, self.default_orientation)
            if not movement_state.is_success:
                if movement_state.is_irrecoverable_state:
                    assert isinstance(movement_state, RobotMovementErrorState)
                    raise RobotMovementFailedException("Could not move to robot target pose. state: %s" % movement_state,
                                                       movement_state)
                else:
                    print('Movement failed, but still in recoverable state.')
        else:
            print('Target position %s (dtype: %s) after action %s out of ee bounds: %s' % (new_xyz, new_xyz.dtype, action[:3], self.allowed_ee_bounds))
            return False

        return True

    def step(self, action) -> Tuple[GoalEnvObservationSpace, float, bool, AugmentedReachingEnvInfoDict]:
        """
        :type action: np.ndarray
        """
        action = np.asarray(action)

        movement_valid = self._execute_action(action)

        observation = self._observe()
        info: AugmentedReachingEnvInfoDict = {
            "violated_ee_bounds": False,
            "movement_failed": True,
            "is_success": False
        }
        done = False

        assert np.allclose(observation["desired_goal"]["end_effector_xyz"], self.goal)

        ee_position = observation["observation"]["end_effector_xyz"]

        if not self.allowed_ee_bounds.contains(ee_position) or not movement_valid:
            done = True
            info["violated_ee_bounds"] = True

        assert np.allclose(observation["desired_goal"]["end_effector_xyz"], self.goal)
        if np.linalg.norm(ee_position - self.goal) <= self.success_distance:
            if self.terminate_on_success:
                done = True
            info["is_success"] = True

        # this may not be increased before we have taken the observation, otherwise the remaining time will be incorrect
        self.steps += 1

        if self.steps >= self.max_episode_length and self.finite_time:
            done = True

        return observation, \
            self.compute_reward(observation["achieved_goal"], observation["desired_goal"], info), \
            done, \
            info

    def compute_reward(self, achieved_goal, desired_goal, info: AugmentedReachingEnvInfoDict) -> float:
        """
        :type achieved_goal: Dict
        :type info: Dict
        :type desired_goal: Dict
        """
        violated_bounds = info['violated_ee_bounds']

        if self.reward_mode is AugmentedReachingEnvRewardMode.SparseSuccessOnly:
            if info["is_success"]:
                return 1
            if violated_bounds:
                return -(self.time_penalty * (self.max_episode_length-self.steps))
            return -self.time_penalty

        if violated_bounds:
            return -self.ee_bound_violation_penalty

        if info["is_success"] is True and self.terminate_on_success:
            return self.success_bonus

        if self.reward_mode is AugmentedReachingEnvRewardMode.NegativeL2Norm:
            return -np.linalg.norm(achieved_goal["end_effector_xyz"] - desired_goal["end_effector_xyz"])

        else:
            raise ValueError(f"Reward mode {self.reward_mode} is unknown.")

    def render(self, mode="human"):
        return self.render_image(mode, 128, 128)

    def render_image(self, mode, width, height):
        raise NotImplementedError()


default_wrappers: List[WrapperDef] = [
    (
        lambda env: ScaledRewardEnv(env, 100),
        lambda values: values.suffix('reward', 'Scaled')
    ),
]


def _generate_starting_setup_function(start_area: gym.spaces.Box) \
        -> Callable[[np.random.RandomState], np.array]:

    def starting_setup_function(random: np.random.RandomState):
        return random.uniform(low=start_area.low, high=start_area.high)

    return starting_setup_function


def _generate_goal_function(goal_area: gym.spaces.Box) \
        -> Callable[[np.array, np.random.RandomState], np.array]:

    def goal_function(_: np.array, random: np.random.RandomState):
        return random.uniform(low=goal_area.low, high=goal_area.high)

    return goal_function


def get_environment_variation_name(
        image_observation: AugmentedReachingEnvImageObservation,
        action_type: AugmentedReachingEnvActionType,
        reward_mode: AugmentedReachingEnvRewardMode,
        version: int) -> EnvironmentId:


    env_id = EnvironmentId('AugmentedReachingFrankaReal', version)

    env_id.append('image_obs', image_observation.name)
    env_id.append('action', action_type.name)
    env_id.append('reward', reward_mode.name)

    return env_id


def get_environment_variation_name_with_wrappers(
        image_observation: AugmentedReachingEnvImageObservation,
        action_type: AugmentedReachingEnvActionType,
        reward_mode: AugmentedReachingEnvRewardMode,
        version: int,
        wrappers: List[WrapperDef]) -> EnvironmentId:

    env_id = get_environment_variation_name(
        image_observation,
        action_type,
        reward_mode,
        version
    )
    apply_env_id_wrapper_combination(env_id, wrappers)

    return env_id


def get_environment_variation(
        image_observation: AugmentedReachingEnvImageObservation,
        action_type: AugmentedReachingEnvActionType,
        reward_mode: AugmentedReachingEnvRewardMode,
        extra_args: AugmentedReachingEnvExtraArguments = None) -> AugmentedReachingEnv:
    extra_args: AugmentedReachingEnvExtraArguments = extra_args or {}

    linear_action_size = extra_args.get('linear_action_size', 0.05)
    start_and_goal_area_margin = linear_action_size * 1.5

    #ee_bounds = gym.spaces.Box(low=np.asarray([-0.1, -0.7, 0.0]), high=np.asarray([0.6, -0.3, 0.5]), dtype=np.float32)
    ee_bounds = gym.spaces.Box(low=np.asarray([0.1, -0.65, 0.0]), high=np.asarray([0.4, -0.35, 0.3]), dtype=np.float32)

    start_area = gym.spaces.Box(low=ee_bounds.low + start_and_goal_area_margin,
                                high=ee_bounds.high - start_and_goal_area_margin)
    goal_area = gym.spaces.Box(low=ee_bounds.low + start_and_goal_area_margin,
                               high=ee_bounds.high - start_and_goal_area_margin)

    max_episode_length = extra_args.get('max_episode_length', 50)
    maximum_time_penalty = extra_args.get('maximum_time_penalty', 1.0)

    image_height = extra_args.get('image_height', 48)
    image_width = extra_args.get('image_width', image_height)

    return AugmentedReachingEnv(
        image_observation=image_observation,
        observation_image_height=image_height,
        observation_image_width=image_width,


        action_type=action_type,
        starting_setup_function=_generate_starting_setup_function(start_area),
        goal_function=_generate_goal_function(goal_area),
        goal_position_space=goal_area,

        reward_mode=reward_mode,
        success_distance=0.025,
        success_bonus=0,
        terminate_on_success=True,
        allowed_ee_bounds=ee_bounds,
        ee_bound_violation_penalty=50,
        max_action_offset=linear_action_size,
        finite_time=True,
        max_episode_length=max_episode_length,
        time_penalty=maximum_time_penalty / max_episode_length,
    )


def get_environment_variation_with_wrappers(
        image_observation: AugmentedReachingEnvImageObservation,
        action_type: AugmentedReachingEnvActionType,
        reward_mode: AugmentedReachingEnvRewardMode,
        wrappers: List[WrapperDef],
        extra_args: AugmentedReachingEnvExtraArguments = None) -> gym.GoalEnv:
    env = get_environment_variation(
        image_observation,
        action_type,
        reward_mode,
        extra_args
    )

    env = apply_env_wrapper_combination(env, wrappers)
    return env


def _register_variation(
        register_function: Callable[[str, Callable[[Dict], gym.GoalEnv]], None],
        image_observation: AugmentedReachingEnvImageObservation,
        action_type: AugmentedReachingEnvActionType,
        reward_mode: AugmentedReachingEnvRewardMode,
        wrappers: List[WrapperDef],
        version: int = 0):

    register_function(
        str(get_environment_variation_name_with_wrappers(
            image_observation=image_observation,
            action_type=action_type,
            reward_mode=reward_mode,
            version=version,
            wrappers=wrappers
        )),

        lambda config: get_environment_variation_with_wrappers(
            image_observation=image_observation,
            action_type=action_type,
            reward_mode=reward_mode,
            wrappers=wrappers,
            extra_args=config))


def _register_variation_permutations(register_function: Callable[[str, Callable[[Dict], gym.GoalEnv]], None]):
    for (image_observation, action_type, reward_mode) in get_variation_permutations():
        for wrapper_combination in get_all_wrapper_combinations(default_wrappers):
            _register_variation(
                register_function=register_function,
                image_observation=image_observation,
                action_type=action_type,
                reward_mode=reward_mode,
                wrappers=wrapper_combination
            )


def get_variation_permutations() -> Iterable[Tuple[
    AugmentedReachingEnvImageObservation,
    AugmentedReachingEnvActionType,
    AugmentedReachingEnvRewardMode
]]:
    for image_observation in AugmentedReachingEnvImageObservation:
        for action_type in AugmentedReachingEnvActionType:
            for reward_mode in AugmentedReachingEnvRewardMode:
                yield image_observation, action_type, reward_mode


def _register_gym_fixed_envs():
    from gym.envs.registration import register

    # Add any special variations here
    pass


def register_gym_reaching_envs():
    _register_variation_permutations(gym_register_function_wrapper)
    _register_gym_fixed_envs()


def register_tune_reaching_envs():
    _register_variation_permutations(tune_register_function_wrapper)

