#!/usr/bin/env python3
import random
from enum import Enum, auto, IntEnum
from typing import Tuple, Any, Dict, Union

import rospy
import gym
import numpy as np

from augmented_reality_robotics.enviroments.augmented_picking_env import AugmentedPickingEnv, \
    AugmentedPickingEnvActionType, AugmentedPickingEnvRewardMode, get_environment_variation
from robot_common_task_tools.env_wrappers.exception_handling import ExceptionHandlingWrapperEnv
from robot_common_task_tools.utils.pid import PDController


def _starting_setup_function(rng: np.random.RandomState) -> Tuple[np.ndarray, np.ndarray]:
    return np.asarray([0.5, 0]), np.asarray([0.5, 0, 0.1])


class State(IntEnum):
    Start = auto()
    ApproachPickStart = auto()
    ApproachBox = auto()
    CloseFingers = auto()
    Ascend = auto()



def main():

    rospy.init_node('test_node', anonymous=True)

    raw_env = get_environment_variation(AugmentedPickingEnvActionType.RelativeEEDiscreteFingerAction, AugmentedPickingEnvRewardMode.SparseSuccessOnly,
                                    extra_args={
                                        'max_episode_length': 50
                                    })

    env = ExceptionHandlingWrapperEnv(raw_env)

    current_state = State.Start
    pick_position = None
    finish_position = None
    controller = PDController(shape=(3,), k_p=10, k_d=0.0, dt=1)

    def generate_action(obs):
        nonlocal current_state, pick_position, finish_position

        ee_pos: np.ndarray = obs["observation"]["end_effector_xyz"]
        box_pos: np.ndarray = obs["observation"]["box_xyz"]
        finger_state: np.ndarray = obs["observation"]["finger_state"][0]

        if current_state == State.Start:
            pick_position = box_pos + np.asarray([0, 0, 0.15])
            finish_position = box_pos + np.asarray([0, 0, 0.3])
            current_state = State.ApproachPickStart
            return np.asarray([0, 0, 0, 1], dtype=np.float32)  # Just open fingers at start

        if current_state == State.ApproachPickStart:
            target = pick_position
        elif current_state == State.Ascend:
            target = finish_position
        else:
            target = box_pos + np.asarray([0, 0, 0.02])


        error: np.ndarray = target - ee_pos
        ee_action = error[:]
        ee_action *= 10
        ee_action = np.clip(ee_action, env.action_space.low[:3], env.action_space.high[:3])

        is_close = np.linalg.norm(error) < 0.02
        if current_state == State.ApproachPickStart and is_close:
            current_state = State.ApproachBox
        elif current_state == State.ApproachBox and is_close:
            current_state = State.CloseFingers
        elif current_state == State.CloseFingers:
            current_state = State.Ascend
            return np.asarray([0, 0, 0, -1], dtype=np.float32)

        return np.asarray([ee_action[0], ee_action[1], ee_action[2], 0], dtype=np.float32)

    while not rospy.is_shutdown():
        current_state = State.Start
        pick_position = None
        finish_position = None
        obs = env.reset()
        i = 0
        done = False
        while not done:
            i += 1
            if rospy.is_shutdown():
                break

            action = generate_action(obs)

            print('It %d: Action: %s' % (i, action), end='', flush=True)
            obs, reward, done, info = env.step(action)
            print('  Reward: %f, info: %s' % (reward, info))





if __name__ == '__main__':
    main()