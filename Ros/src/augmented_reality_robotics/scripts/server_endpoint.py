#!/usr/bin/env python3

#import pydevd_pycharm
#pydevd_pycharm.settrace('localhost', port=11337, stdoutToServer=True, stderrToServer=True, suspend=False)

# Imports
import rospy

from ros_tcp_endpoint import TcpServer

__author__ = 'Fabian Schreier'
__version__ = "0.1.0"
__status__ = "Prototype"

def main():
    ros_node_name = rospy.get_param("/TCP_NODE_NAME", 'TCPServer')
    ros_ip = rospy.get_param("/ROS_IP", '127.0.0.1')
    tcp_server = TcpServer(ros_node_name, tcp_ip=ros_ip)
    rospy.init_node(ros_node_name, anonymous=True)

    tcp_server.start()
    rospy.spin()


if __name__ == "__main__":
    main()