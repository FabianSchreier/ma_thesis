#!/usr/bin/env python3

# Imports
import math
from typing import TypedDict, List

import rospy
import numpy as np
from control_msgs.msg import FollowJointTrajectoryFeedback
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError


from robot_franka_task_envs.features.robots.panda import PandaMoveItFeature, panda_default_ee_transformation
from robot_franka_task_envs.features.robots.generic import EndEffectorTransformationFeature

__author__ = 'Fabian Schreier'
__version__ = "0.1.0"
__status__ = "Prototype"

from robot_franka_task_envs.utils import R


def main():
    rospy.init_node('t_robot_position')

    robot_raw = PandaMoveItFeature()
    robot = EndEffectorTransformationFeature(robot_raw, panda_default_ee_transformation())

    robot.move_to_reset_pose()

    robot.move_to_position_and_orientation((0.5, -0.2, 0.5), R.about_axis(math.pi, (1, 0, 0)))
    rospy.sleep(0.5)
    #while not rospy.is_shutdown():
    for _ in range(3):

        robot.move_to_position_and_orientation((0.5, -0.2, 0.1), R.about_axis(math.pi, (1, 0, 0)))
        rospy.sleep(0.5)
        robot.move_to_position_and_orientation((0.5, 0.2, 0.1), R.about_axis(math.pi, (1, 0, 0)))
        rospy.sleep(0.5)
        robot.move_to_position_and_orientation((0.5, 0.2, 0.5), R.about_axis(math.pi, (1, 0, 0)))
        rospy.sleep(0.5)
        robot.move_to_position_and_orientation((0.5, -0.2, 0.5), R.about_axis(math.pi, (1, 0, 0)))
        rospy.sleep(0.5)

    robot.move_to_position_and_orientation((0.5, -0.2, 0.1), R.about_axis(math.pi, (1, 0, 0)))


if __name__ == '__main__':
    main()

