#!/usr/bin/env python3


import rospy
import tf
from geometry_msgs.msg import PoseStamped, Pose, Point, Quaternion

from augmented_reality_robotics.srv import CameraPose, CameraPoseRequest


def get_transform(from_frame, to_frame, listener: tf.TransformListener = None):
    if listener is None:
        listener = tf.TransformListener()

    while not rospy.is_shutdown():
        try:
            return listener.lookupTransform(from_frame, to_frame, rospy.Time())
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue

def main():
    rospy.init_node('update_camera_pose')

    rospy.loginfo("Waiting for transformation from world to camera")
    (world_to_base_trans, world_to_base_rot) = get_transform('/world', '/camera_base')
    rospy.loginfo("Received transformation %s, %s" % (world_to_base_trans, world_to_base_rot))

    rospy.loginfo("Waiting for transformation from base to rgb")
    (base_to_rgb_trans, bse_to_rgb_rot) = get_transform('/camera_base', '/rgb_camera_link')
    rospy.loginfo("Received transformation %s, %s" % (base_to_rgb_trans, bse_to_rgb_rot))

    pose = PoseStamped(
        pose=Pose(
            position=Point(*world_to_base_trans),
            orientation=Quaternion(*world_to_base_rot)
        )
    )
    pose.header.frame_id = 'world'

    service = rospy.ServiceProxy('/augmented_environment/camera/update', CameraPose)
    rospy.loginfo("Waiting for CameraPose service")
    service.wait_for_service()

    rospy.loginfo("Updating pose")
    response = service.call(CameraPoseRequest(target=pose))
    rospy.loginfo("Done updating camera pose")


if __name__ == "__main__":
    main()