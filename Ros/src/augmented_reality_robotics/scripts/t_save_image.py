#!/usr/bin/env python

# Imports

import rospy
import numpy as np
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError


__author__ = 'Fabian Schreier'
__version__ = "0.1.0"
__status__ = "Prototype"


def main():
    rospy.init_node('t_image_save')

    msg = rospy.wait_for_message('/rgb/image_raw', Image)
    assert isinstance(msg, Image)

    with open('image_data_%s.dat' % msg.encoding, 'wb') as fh:
        fh.write(msg.data)

    cv_bridge = CvBridge()

    array = np.asarray(cv_bridge.imgmsg_to_cv2(msg))

    np.save('image_data.npy', array)


if __name__ == '__main__':
    main()

