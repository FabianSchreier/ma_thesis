#!/usr/bin/env python

# Imports
import rospy
import controller_manager_msgs.srv as cm_srv

import numpy as np
from actionlib import ActionClient, ClientGoalHandle
from actionlib_msgs.msg import GoalStatus
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError


__author__ = 'Fabian Schreier'
__version__ = "0.1.0"
__status__ = "Prototype"

from augmented_reality_robotics.msg import *


GoalStatusMap = {
    GoalStatus.PENDING: "PENDING",
    GoalStatus.ACTIVE: "ACTIVE",
    GoalStatus.PREEMPTED: "PREEMPTED",
    GoalStatus.SUCCEEDED: "SUCCEEDED",
    GoalStatus.ABORTED: "ABORTED",
    GoalStatus.REJECTED: "REJECTED",
    GoalStatus.PREEMPTING: "PREEMPTING",
    GoalStatus.RECALLING: "RECALLING",
    GoalStatus.RECALLED: "RECALLED",
    GoalStatus.LOST: "LOST",
}


def main():
    rospy.init_node('t_image_save')


    client = ActionClient('/TestAction', TestActionAction)
    print('Waiting for server')
    client.wait_for_action_server_to_start()

    print('Sending goal')
    goal = TestActionGoal(goal='Foo')
    handle = client.send_goal(goal, feedback_cb=feedback_cb, transition_cb=transition_cb)

    rospy.spin()


def feedback_cb(gh, feedback):  # type: (ClientGoalHandle, TestActionActionFeedback) -> None
    print('Feedback %s' % (feedback.feedback))


def transition_cb(gh):  # type: (ClientGoalHandle) -> None
    print('New status: %s' % GoalStatusMap[gh.get_goal_status()])
    if gh.get_goal_status() == GoalStatus.SUCCEEDED:
        print('Result: %s' % gh.get_result().result)


if __name__ == '__main__':
    main()

