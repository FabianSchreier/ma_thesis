#!/usr/bin/env python

# Imports
import rospy
import controller_manager_msgs.srv as cm_srv

import numpy as np
from actionlib import ActionClient, ClientGoalHandle
from actionlib_msgs.msg import GoalStatus
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError


__author__ = 'Fabian Schreier'
__version__ = "0.1.0"
__status__ = "Prototype"

from augmented_reality_robotics.features.objects import ObjectState, ObjectStateFeature


def main():
    rospy.init_node('t_object_state')

    osf = ObjectStateFeature()

    os = osf['main']
    with os.update() as mod:
        mod.position = np.asarray([1, 2, 3])

    rospy.spin()


if __name__ == '__main__':
    main()

