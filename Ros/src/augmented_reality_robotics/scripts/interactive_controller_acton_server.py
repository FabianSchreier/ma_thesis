#!/usr/bin/env python3
import actionlib
import control_msgs.msg
import moveit_commander
import numpy as np
import rospy
import sensor_msgs.msg
from actionlib import get_name_of_constant
from control_msgs.msg import FollowJointTrajectoryActionGoal
from typing import Optional, cast, Any, Type

from moveit_msgs.msg import RobotTrajectory, MoveItErrorCodes

from augmented_reality_robotics.msg import *
from augmented_reality_robotics.tolerances import Tolerances
from robot_franka_task_envs.ros.messages import UpdatePublisher


#import pydevd_pycharm
#pydevd_pycharm.settrace('localhost', port=11337, stdoutToServer=True, stderrToServer=True, suspend=False)


def action_constant_to_string(message_type, constant_value):  # type: (Type, int) -> str
    for k, v in message_type.__dict__.items():  # type: str, Any
        if not k.startswith('_') and isinstance(v, int) and v == constant_value:
            return k

    raise ValueError('Given value %d is not a constant of %s' % (constant_value, message_type.__name__))


class ActionState(object):
    def __init__(self, goal_handle):  # type: (ActionState, actionlib.ClientGoalHandle) -> None
        self.goal_handle = goal_handle
        self.is_accepted = False
        self.is_canceled = False
        self.is_terminated = False
        self.terminal_state = None  # type: Optional[int]

        self.reset_goal_handle = None  # type: Optional[actionlib.ClientGoalHandle]

    @property
    def is_successful(self):
        return self.is_terminated and self.terminal_state == actionlib.TerminalState.SUCCEEDED

    def cancel(self):
        self.goal_handle.cancel()
        if self.reset_goal_handle is not None:
            self.reset_goal_handle.cancel()

        self.is_canceled = True


class InteractiveController(object):
    def __init__(self):

        self._incoming_action_goal = None  # type: Optional[actionlib.ServerGoalHandle]
        self._real_env_action_state = None  # type: Optional[ActionState]
        self._sim_env_action_state = None  # type: Optional[ActionState]

        self.sim_robot_joint_position_topic = '/augmented_environment/joint_states/simulated'
        self.status_publisher_topic = '/augmented_environment/status'

        self.incoming_action_server_namespace = 'position_joint_trajectory_controller/follow_joint_trajectory'
        self.real_env_action_server_namespace = 'position_joint_trajectory_controller_backend/follow_joint_trajectory'
        self.sim_env_action_server_namespace = '/augmented_environment/feedback_controller'

        self.tolerances = Tolerances(rospy.get_param('~tolerances'))

        self.status_publisher = UpdatePublisher(self.status_publisher_topic, InteractiveControllerStatus)

        self.incoming_action_server = actionlib.ActionServer(
            self.incoming_action_server_namespace,
            control_msgs.msg.FollowJointTrajectoryAction,
            goal_cb=self._goal_cb, cancel_cb=self._cancel_cb,
            auto_start=False)

        self.real_env_action_client = actionlib.ActionClient(
            self.real_env_action_server_namespace,
            control_msgs.msg.FollowJointTrajectoryAction)

        self.sim_env_action_client = actionlib.ActionClient(
            self.sim_env_action_server_namespace,
            control_msgs.msg.FollowJointTrajectoryAction)

        self.status_publisher.update_message_and_publish(InteractiveControllerStatus(status=InteractiveControllerStatus.STARTING))
        print('Starting ActionServer')
        self.incoming_action_server.start()
        print('Waiting for real env action client to connect')
        self.real_env_action_client.wait_for_action_server_to_start()
        print('Action client connected')
        print('Waiting for sim env action client to connect')
        self.sim_env_action_client.wait_for_action_server_to_start()
        print('Action client connected')

        self.real_robot_move_group = moveit_commander.MoveGroupCommander('panda_arm')

        self.status_publisher.update_message_and_publish(InteractiveControllerStatus(status=InteractiveControllerStatus.ACTIVE))

    def run(self, rate=None):  # type: (Optional[rospy.Rate]) -> None
        if rate is None:
            rate = rospy.Rate(10)

        while not rospy.is_shutdown():
            self.spin_once()
            rate.sleep()

    def spin_once(self):
        self.status_publisher.publish()

    def _cancel_cb(self, goal_handle):  # type: (actionlib.ServerGoalHandle) -> None
        if self._real_env_action_state:
            self._real_env_action_state.cancel()
        if self._sim_env_action_state:
            self._sim_env_action_state.cancel()
        self.status_publisher.update_message_and_publish(InteractiveControllerStatus(status=InteractiveControllerStatus.ACTIVE))

    def _goal_cb(self, goal_handle):  # type: (actionlib.ServerGoalHandle) -> None
        print('Received new goal: %s' % goal_handle)

        self.status_publisher.update_message_and_publish(InteractiveControllerStatus(status=InteractiveControllerStatus.EXECUTING))

        self._incoming_action_goal = goal_handle

        goal = goal_handle.goal  # type: FollowJointTrajectoryActionGoal
        print('Got goal \n %s' % goal.goal)

        goal_with_updated_tolerances = self.tolerances.clone_and_apply(goal.goal)
        print('Updated tolerances: \n%s' % goal_with_updated_tolerances)

        self._real_env_action_state = ActionState(self.real_env_action_client.send_goal(
            goal_with_updated_tolerances if self.tolerances.apply_on_backend else goal.goal,
            transition_cb=self._real_env_transition_cb,
            feedback_cb=self._real_env_feedback_cb
        ))
        self._sim_env_action_state = ActionState(self.sim_env_action_client.send_goal(
            goal_with_updated_tolerances,
            transition_cb=self._sim_env_transition_cb
        ))

    def _terminal_state_update(self, action_state):  # type: (ActionState) -> None
        if self._sim_env_action_state is None or self._real_env_action_state is None:
            return

        comm_state = action_state.goal_handle.get_comm_state()  # Check which state transition happened

        if comm_state == actionlib.CommState.ACTIVE:  # One of the goals was accepted
            action_state.is_accepted = True

            if self._real_env_action_state.is_accepted and self._sim_env_action_state.is_accepted:
                print('Goal accepted')
                self._incoming_action_goal.set_accepted()
            else:
                print('Real-env goal accepted: %s, sim-env goal accepted: %s' % (self._real_env_action_state.is_accepted, self._sim_env_action_state.is_accepted))

        elif comm_state == actionlib.CommState.DONE and not action_state.is_canceled:  # One of the goals was terminated
            terminal_state = action_state.goal_handle.get_terminal_state()

            action_state.is_terminated = True
            action_state.terminal_state = action_state.goal_handle.get_terminal_state()

            if terminal_state != actionlib.TerminalState.SUCCEEDED:
                self.status_publisher.update_message_and_publish(InteractiveControllerStatus(status=InteractiveControllerStatus.RESETTING))

                if action_state.goal_handle.get_comm_state() != actionlib.CommState.ACTIVE:
                    print('Not in active comm state: %s ' % get_name_of_constant(actionlib.CommState, action_state.goal_handle.get_comm_state()))
                print('Setting incoming goal to terminal state')
                self._set_incoming_goal_failed_state(action_state.goal_handle)

                # The execution of one of them failed -> Cancel the other and reset
                if action_state == self._real_env_action_state:
                    print('Running goal failed cause real-env execution failed with terminal state %s' % get_name_of_constant(actionlib.TerminalState, terminal_state))
                    self._reset_sim_robot_to_real_state()
                else:
                    print('Running goal failed cause sim-env execution failed with terminal state %s' % get_name_of_constant(actionlib.TerminalState, terminal_state))
                    self._reset_real_robot_to_sim_state()

                print('Closing running goal')
                self._sim_env_action_state = None
                self._real_env_action_state = None
                self._incoming_action_goal = None
                self.status_publisher.update_message_and_publish(InteractiveControllerStatus(status=InteractiveControllerStatus.ACTIVE))

            elif self._real_env_action_state.is_successful and self._sim_env_action_state.is_successful:
                print('Running goal succeeded')
                self._incoming_action_goal.set_succeeded(action_state.goal_handle.get_result(), action_state.goal_handle.get_goal_status_text())

                print('Closing running goal')
                self._sim_env_action_state = None
                self._real_env_action_state = None
                self._incoming_action_goal = None
                self.status_publisher.update_message_and_publish(InteractiveControllerStatus(status=InteractiveControllerStatus.ACTIVE))
            else:
                print('Real-env goal successful: %s, sim-env goal successful: %s' % (self._real_env_action_state.is_successful, self._sim_env_action_state.is_successful))

    def _remove_finger_states(self, state: sensor_msgs.msg.JointState) -> sensor_msgs.msg.JointState:

        if 'panda_finger_joint1' not in state.name and 'panda_finger_joint2' not in state.name:
            return state

        names = list(state.name)
        pos = np.asarray(state.position)
        vel = np.asarray(state.velocity)
        eff = np.asarray(state.effort)

        mask = np.ones(pos.shape, bool)
        if 'panda_finger_joint1' in state.name:
            mask[state.name.index('panda_finger_joint1')] = False
            names.remove('panda_finger_joint1')
        if 'panda_finger_joint2' in state.name:
            mask[state.name.index('panda_finger_joint2')] = False
            names.remove('panda_finger_joint2')

        pos = pos[mask]
        vel = vel[mask]
        eff = eff[mask]
        return sensor_msgs.msg.JointState(
            name=names,
            position=tuple(pos),
            velocity=tuple(vel),
            effort=tuple(eff)
        )


    def _reset_real_robot_to_sim_state(self):
        if not self._real_env_action_state.is_terminated:
            self._real_env_action_state.cancel()

        last_sim_state = self._remove_finger_states(cast(sensor_msgs.msg.JointState, rospy.wait_for_message(self.sim_robot_joint_position_topic, sensor_msgs.msg.JointState)))
        print('Last sim state for resetting: %s' % last_sim_state)

        success_flag: bool
        trajectory: RobotTrajectory
        planning_time: float
        error_code: MoveItErrorCodes
        success_flag, trajectory, planning_time, error_code = self.real_robot_move_group.plan(last_sim_state)

        print('Constructing simple action client')
        real_env_simple_action_client = actionlib.SimpleActionClient(self.real_env_action_client.ns, self.real_env_action_client.ActionSpec)
        real_env_simple_action_client.action_client = self.real_env_action_client

        print('Executing reset trajectory')
        final_status = self._real_env_action_state.reset_goal_handle = real_env_simple_action_client.send_goal_and_wait(
            control_msgs.msg.FollowJointTrajectoryGoal(
                trajectory=trajectory.joint_trajectory
            )
        )
        print('Finished reset with final status %s' % action_constant_to_string(actionlib.GoalStatus, final_status))

    def _reset_sim_robot_to_real_state(self):
        if not self._sim_env_action_state.is_terminated:
            self._sim_env_action_state.cancel()
        # Nothing more to do here. Sim robot already mirrors real one when there is no goal running

    def _set_incoming_goal_failed_state(self, goal_handle):  # type: (actionlib.ClientGoalHandle) -> None
        terminal_state = goal_handle.get_terminal_state()
        result = goal_handle.get_result()
        result_text = goal_handle.get_goal_status_text()

        if terminal_state == actionlib.TerminalState.ABORTED:
            self._incoming_action_goal.set_aborted(result, result_text)
        elif terminal_state == actionlib.TerminalState.REJECTED:
            self._incoming_action_goal.set_rejected(result, result_text)
        elif terminal_state == actionlib.TerminalState.PREEMPTED \
                or terminal_state == actionlib.TerminalState.RECALLED:
            self._incoming_action_goal.set_canceled(result, result_text)
        else:
            self._incoming_action_goal.set_canceled(result, result_text)

    def _sim_env_transition_cb(self, goal_handle):  # type: (actionlib.ClientGoalHandle) -> None
        if goal_handle != self._sim_env_action_state.goal_handle:
            return
        if not self._incoming_action_goal:
            print('Incoming goal is not longer valid')
            return
        self._terminal_state_update(self._sim_env_action_state)

    def _real_env_transition_cb(self, goal_handle):  # type: (actionlib.ClientGoalHandle) -> None
        if goal_handle != self._real_env_action_state.goal_handle:
            return
        if not self._incoming_action_goal:
            print('Incoming goal is not longer valid')
            return
        self._terminal_state_update(self._real_env_action_state)

    def _real_env_feedback_cb(self, goal_handle, feedback):  # type: (actionlib.ClientGoalHandle, control_msgs.msg.FollowJointTrajectoryFeedback) -> None

        if self._incoming_action_goal:
            self._incoming_action_goal.publish_feedback(feedback)


def main():
    rospy.init_node('interactive_position_joint_trajectory_controller')

    controller = InteractiveController()

    controller.run()


if __name__ == '__main__':
    main()

