#!/usr/bin/env python3

# Imports
import math
from typing import TypedDict, List

import rospy
import numpy as np
from control_msgs.msg import FollowJointTrajectoryActionFeedback
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError


from robot_franka_task_envs.features.robots.panda import PandaMoveItFeature, panda_default_ee_transformation
from robot_franka_task_envs.features.robots.generic import EndEffectorTransformationFeature

__author__ = 'Fabian Schreier'
__version__ = "0.1.0"
__status__ = "Prototype"

from robot_franka_task_envs.utils import R


class DataEntry(TypedDict):

    stamp_sec: int
    stamp_nsec: int

    pos_desired: List[float]
    pos_actual: List[float]
    pos_error: List[float]

    vel_desired: List[float]
    vel_actual: List[float]
    vel_error: List[float]

dtype = np.dtype([
    ('stamp_sec', int),
    ('stamp_nsec', int),
    ('pos_desired', np.float64, (7,)),
    ('pos_actual', np.float64, (7,)),
    ('pos_error', np.float64, (7,)),

    ('vel_desired', np.float64, (7,)),
    ('vel_actual', np.float64, (7,)),
    ('vel_error', np.float64, (7,)),
])


class DataLogger:
    def __init__(self, initial_capacity=5000):
        self.data = np.zeros(dtype=dtype, shape=(initial_capacity,))
        self.index = 0

        self.size = 0

    @property
    def capacity(self) -> int:
        return self.data.shape[0]

    def append(self, entry: DataEntry):
        if self.index == self.capacity:
            self._extend()

        key: str
        for key in self.data.dtype.names:
            val = entry[key]
            if len(self.data[self.index][key].shape) > 1:
                val = np.asarray(val)
            self.data[self.index][key] = val
        self.index += 1
        self.size += 1

    def trim(self):
        self.data = self.data[:self.size]

    def append_message(self, msg: FollowJointTrajectoryActionFeedback):
        entry: DataEntry = {
            'stamp_sec': msg.header.stamp.secs,
            'stamp_nsec': msg.header.stamp.nsecs,
            'pos_desired': msg.feedback.desired.positions,
            'pos_actual': msg.feedback.actual.positions,
            'pos_error': msg.feedback.error.positions,
            'vel_desired': msg.feedback.desired.velocities,
            'vel_actual': msg.feedback.actual.velocities,
            'vel_error': msg.feedback.error.velocities,
        }
        self.append(entry)

    def _extend(self):
        new_data = np.zeros(dtype=dtype, shape=(self.capacity * 2,))
        new_data[:self.capacity] = self.data
        self.data = new_data

def main():
    rospy.init_node('t_robot_position_writer')
    real_data = DataLogger()
    sim_data = DataLogger()

    real_subscriber = rospy.Subscriber('/position_joint_trajectory_controller_backend/follow_joint_trajectory/feedback',
                                       FollowJointTrajectoryActionFeedback, callback=real_data.append_message)
    sim_subscriber = rospy.Subscriber('/augmented_environment/feedback_controller/feedback',
                                      FollowJointTrajectoryActionFeedback, callback=sim_data.append_message)

    rospy.spin()

    real_data_saved = real_data.data[:real_data.size]
    sim_data_saved = sim_data.data[:sim_data.size]

    print('Saving %d entries for real robot' % real_data_saved.shape[0])
    np.save('real.npy', real_data_saved)
    print('Saving %d entries for sim robot' % sim_data_saved.shape[0])
    np.save('sim.npy', sim_data_saved)


if __name__ == '__main__':
    main()

