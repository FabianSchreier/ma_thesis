#!/usr/bin/env python
import random
from enum import Enum, auto, IntEnum
from typing import Tuple

import rospy
import gym
import numpy as np
from ray.tune import register_env

from augmented_reality_robotics.enviroments.augmented_picking_env import AugmentedPickingEnv, \
    AugmentedPickingEnvActionType, AugmentedPickingEnvRewardMode, get_environment_variation, get_environment_variation_name
import ray
import ray.rllib.agents.sac as sac
from ray.tune.logger import pretty_print

from robot_common_task_tools.env_wrappers.exception_handling import ExceptionHandlingWrapperEnv
from robot_common_task_tools.env_wrappers.generic import FlattenDictAdapter


def main():

    rospy.init_node('test_node', anonymous=True)

    name = str(get_environment_variation_name(AugmentedPickingEnvActionType.RelativeEEDiscreteFingerAction, AugmentedPickingEnvRewardMode.NegativeL2NormHeightAndDistance))

    register_env(name, lambda config: FlattenDictAdapter(ExceptionHandlingWrapperEnv(get_environment_variation(
        AugmentedPickingEnvActionType.RelativeEEDiscreteFingerAction,
        AugmentedPickingEnvRewardMode.NegativeL2NormHeightAndDistance, extra_args=config))))

    ray.init()
    config = sac.DEFAULT_CONFIG.copy()
    config["num_gpus"] = 0
    config["num_workers"] = 0
    config["timesteps_per_iteration"] = 100
    config["learning_starts"] = 100
    trainer = sac.SACTrainer(config=config, env=name)

    # Can optionally call trainer.restore(path) to load a checkpoint.
    trainer.restore('/root/ray_results/SAC_AugmentedPickingFrankaReal-RelativeEEDiscreteFingerAction-NegativeL2NormHeightAndDistance-v0_2021-10-27_08-55-13aohc6nz4/checkpoint_000021/checkpoint-21')

    for i in range(1000):
        # Perform one iteration of training the policy with SAC
        print('Iteration %d' % (i+1))
        result = trainer.train()
        print(pretty_print(result))

        if i % 10 == 0:
            checkpoint = trainer.save()
            print("checkpoint saved at", checkpoint)


if __name__ == '__main__':
    main()