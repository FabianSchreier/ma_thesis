#!/usr/bin/env ros_python3

import os
from pathlib import Path
from typing import Optional, Dict

import collections
import ray
import yaml
from ray.rllib import BaseEnv
from ray.rllib.agents.callbacks import DefaultCallbacks
from ray.rllib.evaluation import MultiAgentEpisode
from ray.rllib.evaluation.rollout_worker import RolloutWorker
from ray.rllib.policy.policy import Policy
from ray.rllib.train import create_parser
from ray.rllib.utils import force_list
from ray.rllib.utils.deprecation import deprecation_warning
from ray.rllib.utils.typing import PolicyID
from ray.tune.progress_reporter import CLIReporter, JupyterNotebookReporter
from ray.tune.resources import resources_to_json
from ray.tune.result import *
from ray.tune.schedulers import create_scheduler
from ray.tune.tune import run_experiments

from robot_common_task_tools.env_wrappers.exception_handling import ExceptionHandlingWrapperEnv
from robot_common_task_tools.env_wrappers.generic import FlattenDictAdapter, \
    StepCountingAdapter

IS_NOTEBOOK = False

from augmented_reality_robotics.enviroments.augmented_reaching_env import \
    register_tune_reaching_envs as augm_register_tune_reaching_envs, \
    get_environment_variation as augm_get_environment_variation, AugmentedReachingEnvImageObservation, \
    AugmentedReachingEnvActionType, AugmentedReachingEnvRewardMode
from robot_pyb_task_envs.pybullet.robotics.franka.reaching_env import \
    register_tune_reaching_envs as pyb_register_tune_reaching_envs, \
    get_environment_variation as pyb_get_environment_variation, ReachingEnvImageObservation, ReachingEnvActionType, \
    ReachingEnvGoal, ReachingEnvRewardMode


class EpisodeSuccessCallback(DefaultCallbacks):
    def on_episode_start(self,
                         *,
                         worker: "RolloutWorker",
                         base_env: BaseEnv,
                         policies: Dict[PolicyID, Policy],
                         episode: MultiAgentEpisode,
                         env_index: Optional[int] = None,
                         **kwargs) -> None:
        pass

    def on_episode_end(self,
                       *,
                       worker: "RolloutWorker",
                       base_env: BaseEnv,
                       policies: Dict[PolicyID, Policy],
                       episode: MultiAgentEpisode,
                       env_index: Optional[int] = None,
                       **kwargs) -> None:
        info = episode.last_info_for()
        if 'is_success' in info:
            episode.custom_metrics["is_success"] = int(info['is_success'])

        if 'num_steps' in info:
            episode.custom_metrics["num_steps"] = int(info['num_steps'])


def main():
    from ray.tune import register_env

    register_env("SimReachSparse-v0", lambda config:
    StepCountingAdapter(
        FlattenDictAdapter(
            ExceptionHandlingWrapperEnv(
                pyb_get_environment_variation(
                    ReachingEnvImageObservation.NoImages,
                    ReachingEnvActionType.RelativeEndEffector,
                    ReachingEnvGoal.EndEffectorXYZ,
                    ReachingEnvRewardMode.SparseSuccessOnly,
                    False,
                    extra_args=config
                )
            )
        ), reset_count=False
    ))
    register_env("SimReachDense-v0", lambda config:
    StepCountingAdapter (
        FlattenDictAdapter(
            ExceptionHandlingWrapperEnv(
                pyb_get_environment_variation(
                    ReachingEnvImageObservation.NoImages,
                    ReachingEnvActionType.RelativeEndEffector,
                    ReachingEnvGoal.EndEffectorXYZ,
                    ReachingEnvRewardMode.NegativeL2Norm,
                    False,
                    extra_args=config
                )
            )
        ), reset_count=False
    ))

    register_env("AugReachSparse-v0", lambda config:
    StepCountingAdapter(
        FlattenDictAdapter(
            ExceptionHandlingWrapperEnv(
                augm_get_environment_variation(
                    AugmentedReachingEnvImageObservation.NoImages,
                    AugmentedReachingEnvActionType.RelativeEndEffector,
                    AugmentedReachingEnvRewardMode.SparseSuccessOnly,
                    extra_args=config
                )
            )
        ), reset_count=False
    ))
    register_env("AugReachDense-v0", lambda config:
    StepCountingAdapter(
        FlattenDictAdapter(
            ExceptionHandlingWrapperEnv(
                augm_get_environment_variation(
                    AugmentedReachingEnvImageObservation.NoImages,
                    AugmentedReachingEnvActionType.RelativeEndEffector,
                    AugmentedReachingEnvRewardMode.NegativeL2Norm,
                    extra_args=config
                )
            )
        ), reset_count=False
    ))

    pyb_register_tune_reaching_envs()
    augm_register_tune_reaching_envs()

    parser = create_parser()
    args = parser.parse_args()
    run(args, parser)


def run(args, parser):
    if args.config_file:
        with open(args.config_file) as f:
            experiments = yaml.safe_load(f)
    else:
        # Note: keep this in sync with tune/config_parser.py
        experiments = {
            args.experiment_name: {  # i.e. log to ~/ray_results/default
                "run": args.run,
                "checkpoint_freq": args.checkpoint_freq,
                "checkpoint_at_end": args.checkpoint_at_end,
                "keep_checkpoints_num": args.keep_checkpoints_num,
                "checkpoint_score_attr": args.checkpoint_score_attr,
                "local_dir": args.local_dir,
                "resources_per_trial": (
                    args.resources_per_trial and
                    resources_to_json(args.resources_per_trial)),
                "stop": args.stop,
                "config": dict(args.config, env=args.env),
                "restore": args.restore,
                "num_samples": args.num_samples,
                "upload_dir": args.upload_dir,
            }
        }

    for exp in experiments.values():
        callbacks = exp.setdefault("config", {}).get("callbacks")

        exp["config"]["callbacks"] = EpisodeSuccessCallback


    # Ray UI.
    if args.no_ray_ui:
        deprecation_warning(old="--no-ray-ui", new="--ray-ui", error=False)
        args.ray_ui = False

    verbose = 1
    for exp in experiments.values():
        # Bazel makes it hard to find files specified in `args` (and `data`).
        # Look for them here.
        # NOTE: Some of our yaml files don't have a `config` section.
        input_ = exp.get("config", {}).get("input")
        if input_ and input_ != "sampler":
            inputs = force_list(input_)
            # This script runs in the ray/rllib dir.
            rllib_dir = Path(__file__).parent

            def patch_path(path):
                if os.path.exists(path):
                    return path
                else:
                    abs_path = str(rllib_dir.absolute().joinpath(path))
                    return abs_path if os.path.exists(abs_path) else path

            abs_inputs = list(map(patch_path, inputs))
            if not isinstance(input_, list):
                abs_inputs = abs_inputs[0]

            exp["config"]["input"] = abs_inputs

        if not exp.get("run"):
            parser.error("the following arguments are required: --run")
        if not exp.get("env") and not exp.get("config", {}).get("env"):
            parser.error("the following arguments are required: --env")

        if args.torch:
            deprecation_warning("--torch", "--framework=torch")
            exp["config"]["framework"] = "torch"
        elif args.eager:
            deprecation_warning("--eager", "--framework=[tf2|tfe]")
            exp["config"]["framework"] = "tfe"
        elif args.framework is not None:
            exp["config"]["framework"] = args.framework

        if args.trace:
            if exp["config"]["framework"] not in ["tf2", "tfe"]:
                raise ValueError("Must enable --eager to enable tracing.")
            exp["config"]["eager_tracing"] = True

        if args.v:
            exp["config"]["log_level"] = "INFO"
            verbose = 3  # Print details on trial result
        if args.vv:
            exp["config"]["log_level"] = "DEBUG"
            verbose = 3  # Print details on trial result

    if args.ray_num_nodes:
        # Import this only here so that train.py also works with
        # older versions (and user doesn't use `--ray-num-nodes`).
        from ray.cluster_utils import Cluster
        cluster = Cluster()
        for _ in range(args.ray_num_nodes):
            cluster.add_node(
                num_cpus=args.ray_num_cpus or 1,
                num_gpus=args.ray_num_gpus or 0,
                object_store_memory=args.ray_object_store_memory)
        ray.init(address=cluster.address)
    else:
        ray.init(
            include_dashboard=args.ray_ui,
            address=args.ray_address,
            object_store_memory=args.ray_object_store_memory,
            num_cpus=args.ray_num_cpus,
            num_gpus=args.ray_num_gpus,
            local_mode=args.local_mode)

    if IS_NOTEBOOK:
        progress_reporter = JupyterNotebookReporter(
            overwrite=verbose >= 3, print_intermediate_tables=verbose >= 1)
    else:
        progress_reporter = CLIReporter(
            metric_columns=collections.OrderedDict({
                TRAINING_ITERATION: "iter",
                TIME_TOTAL_S: "total time (s)",
                TIMESTEPS_TOTAL: "ts",
                "custom_metrics/is_success_mean": "success_rate",
                "episode_len_mean": "#actions_mean",
            }),
            metric="custom_metrics/is_success_mean",
            mode="max",
            print_intermediate_tables=verbose >= 1
        )

    run_experiments(
        experiments,
        scheduler=create_scheduler(args.scheduler, **args.scheduler_config),
        resume=args.resume,
        queue_trials=args.queue_trials,
        verbose=verbose,
        progress_reporter=progress_reporter,
        concurrent=True)

    ray.shutdown()


if __name__ == '__main__':
    main()

