import math

import numpy as np

import matplotlib.pyplot as plt

def main():
    sim = np.load('sim.npy')
    real = np.load('real.npy')

    sim_time = sim['stamp_sec'] + sim['stamp_nsec'] / 1000000000.
    sim_avr_pos_error = np.rad2deg(np.mean(np.abs(sim['pos_error']), axis=-1))

    real_time = real['stamp_sec'] + real['stamp_nsec'] / 1000000000.
    real_avr_pos_error = np.rad2deg(np.mean(np.abs(real['pos_error']), axis=-1))

    start_time = min(np.min(sim_time), np.min(real_time))

    sim_time = sim_time - start_time
    real_time = real_time - start_time


    plt.plot(real_time, real_avr_pos_error, label="Real Robot")
    plt.plot(sim_time, sim_avr_pos_error, label="Simulated Robot")
    plt.legend()
    plt.xlabel("Time (s)")
    plt.ylabel("Mean position error (°)")
    #plt.show()

    plt.savefig("movement_error.png", dpi=300)

    print(f"SIM: mean: %s, min: %f, max: %f, Q1: %f, Q3: %f" % (
        np.mean(sim_avr_pos_error), np.min(sim_avr_pos_error), np.max(sim_avr_pos_error), np.quantile(sim_avr_pos_error, 0.25), np.quantile(sim_avr_pos_error, 0.75)))
    print(f"REAL: mean: %s, min: %f, max: %f" % (
        np.mean(real_avr_pos_error), np.min(real_avr_pos_error), np.max(real_avr_pos_error)))


if __name__ == '__main__':
    main()