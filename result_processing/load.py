# Imports


__author__ = 'Fabian Schreier'
__version__ = "0.1.0"
__status__ = "Prototype"

import argparse
import json

import numpy as np
from pathlib import Path
from typing import List, Any, Dict, Callable
import csv

parameters_file = Path('params.json')
results_file = Path('result.json')
progress_file = Path('progress.csv')


ddpg_trial_type = np.dtype([
    ('trial_id', '<U128'),
    ('path', '<U1024'),

    ('timesteps_total', np.int),
    ('success_rate', np.float32),
    ('episode_length', np.float32),
    ('episodes', np.int),
    ('actions_total', np.int),
    ('reward', np.float32),
    ('done', np.bool),

    ('actor_hiddens', '<U128'),
    ('critic_hiddens', '<U128'),
    ('buffer_size', np.int),
    ('train_batch_size', np.int),
    ('training_intensity', np.int),
])

ppo_trial_type = np.dtype([
    ('trial_id', '<U128'),
    ('path', '<U1024'),

    ('timesteps_total', np.int),
    ('success_rate', np.float32),
    ('episode_length', np.float32),
    ('episodes', np.int),
    ('actions_total', np.int),
    ('reward', np.float32),
    ('done', np.bool),

    ('num_sgd_iter', np.int),
    ('train_batch_size', np.int),
    ('sgd_minibatch_size', np.int),
    ('fcnet_hiddens', '<U128'),
])


def find_folders(algorithm: str, root: Path = None) -> List[Path]:
    if root is None:
        root = Path('../ray_results')

    res = []

    for p in root.glob('**/result.json'):
        if not algorithm.upper()+"_" in str(p):
            continue
        res.append(p.parent)

    return res


def get_last_csv_line(csv_file_path: Path) -> Dict[str, Any]:
    with open(csv_file_path) as fh:
        reader = csv.DictReader(fh)

        last_line = None
        for row in reader:
            if row:
                last_line = row

    return last_line


def _load_ddpg_entry(folder: Path) -> Dict[str, Any]:
    #last_result = get_last_csv_line(folder.joinpath(progress_file))
    #with open(folder.joinpath(parameters_file)) as fh:
    #    params = json.load(fh)

    results = []
    with open(folder.joinpath(results_file)) as fh:
        for line in fh.readlines():
            results.append(json.loads(line))

    last_result = results[-1]
    params = last_result['config']

    iterations_episode_lengths: List[List[int]] = [r['hist_stats']['episode_lengths'] for r in results]
    all_episode_lengths = np.asarray([e for it in iterations_episode_lengths for e in it])
    total_actions = np.sum(all_episode_lengths)

    res = {
        'trial_id': last_result['trial_id'],
        'path': str(folder.resolve()),

        'timesteps_total': int(last_result['timesteps_total']),
        'success_rate': float(last_result['custom_metrics']['is_success_mean']),
        'episode_length': float(last_result['episode_len_mean']),
        'episodes': len(iterations_episode_lengths),
        'actions_total': total_actions,
        'reward': float(last_result['episode_reward_mean']),
        'done': bool(last_result['done']),


        'actor_hiddens': '[%s]' % (', '.join(str(v) for v in params['actor_hiddens'])),
        'critic_hiddens': '[%s]' % (', '.join(str(v) for v in params['critic_hiddens'])),
        'buffer_size': int(params['buffer_size']),
        'train_batch_size': int(params['train_batch_size']),
        'training_intensity': int(params['training_intensity'])
    }
    return res


def _load_ppo_entry(folder: Path) -> Dict[str, Any]:
    #last_result = get_last_csv_line(folder.joinpath(progress_file))
    #with open(folder.joinpath(parameters_file)) as fh:
    #    params = json.load(fh)

    results = []
    with open(folder.joinpath(results_file)) as fh:
        for line in fh.readlines():
            results.append(json.loads(line))

    last_result = results[-1]
    params = last_result['config']

    iterations_episode_lengths: List[List[int]] = [r['hist_stats']['episode_lengths'] for r in results]
    all_episode_lengths = np.asarray([e for it in iterations_episode_lengths for e in it])
    total_actions = np.sum(all_episode_lengths)

    res = {
        'trial_id': last_result['trial_id'],
        'path': str(folder.resolve()),

        'timesteps_total': int(last_result['timesteps_total']),
        'success_rate': float(last_result['custom_metrics']['is_success_mean']),
        'episode_length': float(last_result['episode_len_mean']),
        'episodes': len(iterations_episode_lengths),
        'actions_total': total_actions,
        'reward': float(last_result['episode_reward_mean']),
        'done': bool(last_result['done']),

        'fcnet_hiddens': '[%s]' % (', '.join(str(v) for v in params['model']['fcnet_hiddens'])),
        'num_sgd_iter': int(params['num_sgd_iter']),
        'train_batch_size': int(params['train_batch_size']),
        'sgd_minibatch_size': int(params['sgd_minibatch_size'])
    }
    return res


def load(algorithm: str, dtype: np.dtype, load_function: Callable[[Path], Dict[str, Any]], root: Path = None) -> np.ndarray:
    trial_folders = find_folders(algorithm, root)
    res = np.ndarray(shape=(len(trial_folders),), dtype=dtype)

    for i, trial_folder in enumerate(trial_folders):
        print('Processing %s' % str(trial_folder))
        data = load_function(trial_folder)
        res[i] = tuple(data[key] for key in res.dtype.names)

    return res


def main(args: argparse.Namespace):
    root = None
    if args.root is not None:
        root = Path(args.root)

    ddpg_trials = load('ddpg', ddpg_trial_type, _load_ddpg_entry, root=root)
    np.save("ddpg.npy", ddpg_trials)

    ppo_trials = load('ppo', ppo_trial_type, _load_ppo_entry, root=root)
    np.save("ppo.npy", ppo_trials)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('root', default=None)

    args = parser.parse_args()

    main(args)

