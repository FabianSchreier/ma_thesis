import numpy as np


def rank_configurations():
    #ddpg = np.load('ddpg.npy')

    #ddpg_successfull: np.ndarray = ddpg[ddpg['success_rate'] >= 0.8]
    #ddpg_params = ddpg_successfull[['actor_hiddens', 'critic_hiddens', 'buffer_size', 'train_batch_size', 'training_intensity']]


    ppo = np.load('ppo.npy')
    parameter_fields = ['num_sgd_iter', 'train_batch_size', 'sgd_minibatch_size', 'fcnet_hiddens']

    ppo_successfull: np.ndarray = ppo[ppo['success_rate'] >= 0.8]
    ppo_params = ppo_successfull[parameter_fields]

    data = ppo
    successfull = ppo_successfull
    params = ppo_params

    single_success = set()
    double_success = set()
    count = {}
    for p in params:
        t = tuple(p)

        if t in single_success:
            single_success.remove(t)
            double_success.add(t)
        else:
            single_success.add(t)

    for p in double_success:
        print('Double successful params: %s' % str(p))
    print()
    print()

    for p in single_success:
        print('Single successful params: %s' % str(p))
    print()
    print()

    sorted = np.sort(successfull, order='actions_total')


    print('Total trials: %d, successfull trials: %d, single success: %d, double success: %d' % (
    data.shape[0], successfull.shape[0], len(single_success), len(double_success)))

    print('Best 5 variants: ')
    for best in sorted[:10]:
        t = tuple(best[parameter_fields])

        indices = np.where(sorted[parameter_fields] == best[parameter_fields])[0]

        print('%s with success_rate %f, actions_total %d and timesteps_total %d' % (str(t), best['success_rate'], best['actions_total'], best['timesteps_total']))


        if len(indices) > 1:
            other = sorted[indices[1]]
            print('    Other entry at index %d with success_rate %f and actions_total %d timesteps_total %d' % (indices[1], other['success_rate'], other['actions_total'], other['timesteps_total']))
        else:
            other = None


def print_statistic(array: np.ndarray, field: str):
    #a = array[field]

    #std = np.std(a)
    #mean = np.mean(a)
    #filtered = array[np.logical_and(a > (mean - 2*std), a < (mean + 2*std))]

    print('%s: Mean: %f, Min %f, Q1: %f, Q3: %f, Max: %f' % (
        field,
        np.mean(array),
        np.min(array),
        np.quantile(array, 0.25),
        np.quantile(array, 0.75),
        np.max(array),
    ))


def statistical_analysis():
    ppo = np.load('ppo.npy')
    print('Total runs: %d' % (len(ppo)))

    std = np.std(ppo['timesteps_total'])
    mean = np.mean(ppo['timesteps_total'])
    #filtered = ppo[np.logical_and(ppo['timesteps_total'] > (mean - 2*std), ppo['timesteps_total'] < (mean + 2*std))]
    #filtered = ppo[ppo['timesteps_total'] < (mean + 2 * std)]

    filtered = ppo[ppo['timesteps_total'] < 200000]
    #filtered = ppo

    print_statistic(filtered['timesteps_total'], 'timesteps_total')
    print_statistic(filtered['actions_total'], 'actions_total')
    print_statistic(filtered['actions_total'] / 60 / 60, 'hours')
    print_statistic(filtered['actions_total'] - filtered['timesteps_total'], 'difference')
    print_statistic(filtered['actions_total'] / filtered['timesteps_total'], 'fraction')


if __name__ == '__main__':
    statistical_analysis()
